# Open Humidity Controller
This is the repository of the Open Humidity Controller, an open source humidity controller designed at the University of Geneva.

The architecture consists of two programs:
* a server running inside the humidity controller
* A graphical user interface on a distant windows machine connected on the same network as the humidity controller.

## Server-side

The server-side software is composed of nine parallel processes that control the sysem; they all share a memory presents in the file *main.py*.

The primary process is *p_webServer.py* which manage the write/read access to the memory and the different sensors/controllers using a REST/JSON API.

Most of the other processes are simple functions that allow the controls of the different elements of the controller: 
* The sensors 
* The mass flow controllers
* The PID
* The ultrasonic nebulizer
* The relays and potentiometer

The controller only keep one single state in the memory, but each second the state of the system is recorded in a .csv file for later use by the *p_writeFile* process.

Each of the processes can be easily modified to accommodate other sub-systems. 

## Client-side

The client-side software is a simple GUI which allow the control of the PID parameters and setpoint. It has a live visualization of the evolution of the humidity. It is written in .NET and uses JavaScript to enable some automation of specific behaviour of the controller. 

# How to run it?
## Setup
First, put the MultiProcServer folder on the raspberry machine, and run *main.py*.

The server start and give you a message with the IP of the server. Keep it in mind since it will be necessary during the configuration of the client-side.

Then transfer the executablie in "LocalControl/GUI/HumidorControlPro/bin" to your client machine and launch it. You will need to enter the IP and port from the server and press "Connect".

## Client-side controls

![Alt text](humidor_clientSide.png?raw=true&s=0 "Client-side interface")

### Sensors acquisition
The first box on the top left interface consists of the measured parameters of the sensors and flow controllers. You can also see the web-parsed data from the Web. 

Note that the dewpoint (DP) and the part per million in volume (PPMv) use the pressure collected from the Web, and is exact only for Geneva, Switzerland. If you want to use another value of the pressure, you will need to modify *p_webAcquisition.py* to parse a local weather webpage or implement your own pressure sensor.

### Control parameters
The second box gives the values of the control parameters that are currently in the Humidor. They can be copied for modification by using the arrow pointing to the right. This will copy the parameters to the left box.

### Parameters modification
The left box allows the activation of the PID and the modification of the setpoints. To apply the modifications, you need to press the left-pointing arrow.

If the PID is off, you can only control the wet and dry flow values.

If the PID is on, the flow values are not modifiable, but you can control the PID setpoint and the P, I, D values.

The parameters of "Ctrl Signal" are the following:

* sensors.inj.RH: Use the relative humidity at the injection as the PID parameter.
* sensors.exh.RH: Use the relative humidity at the exhaust as the PID parameter.
* sensors.inj.AH: Use the absolute humidity at the injection as the PID parameter.
* sensors.exh.AH: Use the absolute humidity at the exhaust as the PID parameter.

If you have a sensor inside the measuring cell, you can use the following parameters:
* sensors.cell.RH
* sensors.cell.AH

If you don't have a cell sensor and want to define a setpoint for the value at the cell, you will need to calibrate your setup and use the "Advanced" tab. To calibrate the installation, you need a linear fit of the absolute humidity at the injection vs the absolute humidity at a test cell. For our small cell, we had a linear fitting of y = 0.95x + 0.57.

You then just had to enter the desired RH and the measured temperature at your cell, then press "CALCULATE", then "SEND TO PID".

The last tab is the "system" tab and allows to turn on/off the presence of a cell sensor. It also allows to turn on or off the fans and the atomizer. If necessary, you can also modify the power given to the atomizer by changing the "potentiometer" value and the pulse width modulation parameters.

## Recover the data

To recover the raw data it is necessary to have access to the Humidor, each time the *main.py* is launch it will create a csv file named using the YYYY-MM-DD_HH.mm.ss.ms format, saving all the parameters each second. 

So it is advised after each experience to relaunch the *main.py* file to obtain a new .csv file and to not let run the server when not in use to avoid the use of memory space on the raspberry pi.

# How to built it
## List of components

|  Qty |            Component name            |  Qty |            Component name            |
|:--:|:---------------------------------------:|:--:|:---------------------------------------:|
| 1 |          [Raspberry Pi 3](https://www.digikey.ch/product-detail/fr/raspberry-pi/RASPBERRY-PI-3/1690-1000-ND/6152799)         | 20 |                 [spacers](https://www.distrelec.ch/fr/entretoise-18-mm-mm-no-brand-distin4070m-18/p/11076302?track=true&no-cache=true)                 |
| 1 |          [Pi board ADC-DAC](https://www.digikey.com/products/en?mpart=PIS-1350&v=1910)          | 10 |        [SMC tube connectors (4mm)](https://www.distrelec.ch/fr/straight-plug-in-connector-m5-mm-smc-kq2s04-m5a/p/11030145?track=true&no-cache=true)        |
| 1 |         [Nebulizer NB-80E-01](https://www.digikey.com/products/en?keywords=445-4831-ND)        |  8 |  [SMC Bulk-head plug-in connector (4mm)](https://www.distrelec.ch/fr/bulkhead-plug-in-connector-mm-smc-kq2e08-00a/p/11030298?channel=b2c&price_gs=8.5083&wt_mc=ch.cse.gshop.fr.-&source=googleps&ext_cid=shgooaqchfr-blcss&kw=%7Bkeyword%7D&gclid=Cj0KCQjwzunmBRDsARIsAGrt4mvFTEBeGfQA1Yvtl2ybpnFe66-yS14_loYFYf3wp3eqSBFgQlwMB28aAnF-EALw_wcB)  |
| 1 | plexiglass cylinder 24mm diameter (inner) |  2 |      [SMC T-Plug-In connector (4mm)](https://www.distrelec.ch/fr/plug-in-connector-mm-smc-kq2t04-00a/p/11030194?track=true&no-cache=true)      |
| 1 |      [AC/DC Converter LPT100-M](https://www.digikey.com/products/en?keywords=454-1480-ND)      |  3 | [flanged plastic enclosure (35x50x20 mm)](https://www.distrelec.ch/fr/miniature-flanged-plastic-enclosure-35-35-20-mm-abs-noir-ip-54-hammond-1551mflbk/p/15030267?track=true&no-cache=true) |
| 1 |          [Modular Rack 19’’](https://www.distrelec.ch/fr/rack-modulaire-19-he-84-te-nvent-schroff-24563-174/p/11074433?track=true&no-cache=true)         |  1 |          [Micro-usb wall adapter](https://www.digikey.ch/product-detail/fr/raspberry-pi/T5989DV/1690-1022-ND/6674285)         |
| 1 |    [custom build front rack panel](https://www.digikey.ch/products/fr?keywords=1528-1659-ND)   | 3m |           [8-wires ribon cable](https://www.digikey.com/product-detail/en/assmann-wsw-components/AWG28-08-F-1-300/AE08B-300-ND/2391611)           |
| 3 |   [Sht75 – Digital humidity sensor](https://www.digikey.ch/products/fr?keywords=sht75)  | 4m |           [small electric cable](https://www.digikey.com/product-detail/en/jonard-tools/KSW28BLK-0100/K751-ND/5956067)          |
| 2 |    [Mass flow controller – PiMFC](https://www.ebay.fr/sch/i.html?_from=R40&_trksid=p2380057.m570.l1313.TR12.TRC2.A0.H0.Xpi-mfc.TRS0&_nkw=pi-mfc&_sacat=0)    | 3m |            [water tubing (4mm)](https://www.fishersci.ch/shop/products/k70-silicon-hoses-6/10726352)           |
| 2 |           latching relay           |  1 |              [ethernet cable](https://www.distrelec.ch/fr/interface-de-service-rj45-harting-09-45-452-1504/p/11025872?track=true&no-cache=true)             |
| 1 |               [digipot](https://www.digikey.ch/product-detail/fr/mikroelektronika/MIKROE-2332/1471-1624-ND/6137612)              |  1 |                [hdmi cable](https://www.distrelec.ch/fr/interface-de-service-rj45-harting-09-45-452-1504/p/11025872?track=true&no-cache=true)               |
| 2 |              [fan 80mm](https://www.distrelec.ch/fr/ventilateur-axial-dc-80x80x25mm-12v-61-62m-micronel-c8025h12bplb2/p/30068214?track=true&no-cache=true)              |  1 |            Usb mouse/keyboard           |
| 1 |        [female electric plug](https://www.distrelec.ch/fr/fiche-pour-appareil-c20-faston-mm-16-250-vac-noir-vis-de-montage-pe-schurter-ec11-0021-001-21/p/11041937?track=true&no-cache=true)        |    |                                         |

## Electrical and fluid diagrams

![Alt text](FlowDiagram.png?raw=true&s=0 "Client-side interface")


![Alt text](ElectricDiagram.png?raw=true&s=0 "Client-side interface")

Note: The GPIO pins used for the sensors are the following:
Injection sensor: BMC26 and BCM5 (pins 37 and 29)
Exhaust sensor: BMC13 and BCM6 (pins 33 and 31)
Cell sensor: BCM3 and BCM2 (pins 3 and 5)

If you want to change the layout, you will need to change the following lines in *f_humidor.py* :
```python
self.sht75inj = Sht(26, 5)
self.sht75exh = Sht(13, 6)
self.sht75cell = Sht(3, 2)
```


