var scriptState = 0;
//Etat 0 = init
//Etat 1 = setpoint to 100 and PID on
//Etat 2 = wait till RH > 80
//Etat 3 = setpoint to 0
//Etat 4 = wait till RH < 10
//Etat 5 = PID off and return

function mainLoop() {


  switch(scriptState){
    case 0:
           scriptState = 1;
           break;
    case 1:
           humidorPIDSetpoint(100);
           if (humidorParameterObject["system"]["PID"]["activated"] == 0){
             humidorPIDToggle();
           }
           scriptState = 2;
           break;
    case 2:
           if (humidorParameterObject["sensors"]["inj"]["RH"] > 80){
             scriptState = 3;
           }
           break;
    case 3:
           humidorPIDSetpoint(0);
           if (humidorParameterObject["system"]["PID"]["activated"] == 0){
             humidorPIDToggle();
           }
           scriptState = 4;
           break;
    case 4:
           if (humidorParameterObject["sensors"]["inj"]["RH"] < 10){
             scriptState = 5;
           }
           break;
    case 5:
           if (humidorParameterObject["system"]["PID"]["activated"] == 1){
             humidorPIDToggle();
           }
           clearInterval(mainLoop.timeout);
           return;
           break;
    default:
           scriptState = 0;
           break;
  }
}

mainLoop.timeout = setInterval(mainLoop, 1000);
