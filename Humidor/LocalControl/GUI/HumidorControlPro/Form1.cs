﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jint;
using Jint.Ex;


namespace HumidorControlPro
{

    
    public partial class Form1 : Form
    {
        private WebClient humidorConn;
        private dynamic parameters_ro;
        private Timer updateTimer;
        private bool humidorConnected = false;
        private AsyncronousEngine scriptInterpreter;        
        private double timestart;


        public Form1()
        {
            InitializeComponent();
            textSendAHPID.Enabled = false;
            
            textNeededAH.Enabled = false;
            scriptInterpreter = new AsyncronousEngine();
            scriptInterpreter.SetValue("humidorPIDToggle", new Action(do_humidorPIDToggle));
            scriptInterpreter.SetValue("humidorPIDSetpoint", new Action<object>(do_humidorPIDSetpoint));
            scriptInterpreter.SetValue("humidorParameterSet", new Action<object, object>(do_humidorParameterSet));
            scriptInterpreter.SetValue("humidorParameterObject", parameters_ro);
            scriptInterpreter.SetValue("print", new Action<object>(scriptOutputAdd));


        }

        delegate void scriptPIDSetpointCallback(object s);

        private void do_humidorPIDSetpoint(object s)
        {
            if (this.scriptOutput.InvokeRequired)
            {
                scriptPIDSetpointCallback d = new scriptPIDSetpointCallback(do_humidorPIDSetpoint);
                this.Invoke(d, new object[] { s });
            }
            else
            {
                this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/setpoint/set/", s.ToString()));
            }
        }

        delegate void scriptOutputAddCallback(object s);

        private void scriptOutputAdd(object s)
        {
            if (this.scriptOutput.InvokeRequired)
            {
                scriptOutputAddCallback d = new scriptOutputAddCallback(scriptOutputAdd);
                this.Invoke(d, new object[] { s });
            }
            else
            {
                this.scriptOutput.Text = string.Concat("[", DateTime.Now.ToString(), "] ", s.ToString(), Environment.NewLine, scriptOutput.Text);
            }
        }

        private void humidorConnect_Click(object sender, EventArgs e)
        {
            try
            {
                this.humidorConn = new WebClient();
                var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/parameters"));

                string[] tmp = contents.Split(new[] { "<body>" }, StringSplitOptions.None);
                string[] tmp2 = tmp[1].Split(new[] { "</body>" }, StringSplitOptions.None);
                dynamic parameters = Newtonsoft.Json.JsonConvert.DeserializeObject(tmp2[0]);
                this.parameters_ro = parameters;

                debugTextBox.Text = tmp2[0];

                this.UpdateFormControls();
                this.humidorConnect.Enabled = false;
                this.humidorDisconnect.Enabled = true;
                this.updateTimer.Start();
                this.humidorConnected = true;

                if (Convert.ToInt32(this.parameters_ro.system.PID.activated) == 1)
                {
                    this.humidorWetNew.Enabled = false;
                    this.humidorDryNew.Enabled = false;
                }
                else
                {
                    this.humidorWetNew.Enabled = true;
                    this.humidorDryNew.Enabled = true;
                }


            }
            catch
            {
                MessageBox.Show("System.Net.WebClient INIT ERROR");
            }
        }

        private void updatetimer_tick(object sender, EventArgs e)
        {
            try
            {
                var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/parameters"));

                string[] tmp = contents.Split(new[] { "<body>" }, StringSplitOptions.None);
                string[] tmp2 = tmp[1].Split(new[] { "</body>" }, StringSplitOptions.None);
                dynamic parameters = Newtonsoft.Json.JsonConvert.DeserializeObject(tmp2[0]);
                this.parameters_ro = parameters;
                scriptInterpreter.SetValue("humidorParameterObject", parameters_ro);

                debugTextBox.Text = tmp2[0];

                this.UpdateFormControls();


                double milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                double time = ((milliseconds - timestart) / 1000.0);

                this.humidorRHChart.Series["INJ RH"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.inj.RH));
                this.humidorTChart.Series["INJ T"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.inj.T));
                this.humidorAHChart.Series["INJ AH"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.inj.AH));
                this.humidorPPMChart.Series["INJ PPM"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.inj.ppmv));
                                
                this.humidorRHChart.Series["EXH RH"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.exh.RH));
                this.humidorTChart.Series["EXH T"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.exh.T));
                this.humidorAHChart.Series["EXH AH"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.exh.AH));
                this.humidorPPMChart.Series["EXH PPM"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.exh.ppmv));


                if (Convert.ToInt32(this.parameters_ro.system.activation.cell) == 1)
                {
                    this.humidorRHChart.Series["CELL RH"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.cell.RH));
                    this.humidorTChart.Series["CELL T"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.cell.T));
                    this.humidorAHChart.Series["CELL AH"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.cell.AH));
                    this.humidorPPMChart.Series["CELL PPM"].Points.AddXY((milliseconds - timestart) / 1000.0, Convert.ToDouble(this.parameters_ro.sensors.cell.ppmv));
                }

                this.trackBar1.Maximum = Convert.ToInt32(time);
                this.humidorRHChart.ChartAreas["ChartArea1"].AxisX.Minimum = this.trackBar1.Value;

                this.trackBar2.Maximum = Math.Max(Convert.ToInt32(time)-1, 0);
                this.humidorTChart.ChartAreas["ChartArea1"].AxisX.Minimum = this.trackBar2.Value;

                this.trackBar3.Maximum = Math.Max(Convert.ToInt32(time) - 1, 0);
                this.humidorTChart.ChartAreas["ChartArea1"].AxisX.Minimum = this.trackBar3.Value;

                this.trackBar4.Maximum = Math.Max(Convert.ToInt32(time) - 1, 0);
                this.humidorTChart.ChartAreas["ChartArea1"].AxisX.Minimum = this.trackBar4.Value;

                int start = (int)this.humidorRHChart.ChartAreas[0].AxisX.Minimum;
                int end = (int)this.humidorRHChart.ChartAreas[0].AxisX.Maximum;
             
                double[] temp = this.humidorRHChart.Series["INJ RH"].Points.Where((x, i) => i >= start && i <= end).Select(x => x.YValues[0]).ToArray();
                double ymin = temp.Min();
                double ymax = temp.Max();
                
                double[] temp2 = this.humidorRHChart.Series["EXH RH"].Points.Where((x, i) => i >= start && i <= end).Select(x => x.YValues[0]).ToArray();
                double ymin2 = temp2.Min();
                double ymax2 = temp2.Max();
                double ymin3, ymax3;

                if (Convert.ToInt32(this.parameters_ro.system.activation.cell) == 1)
                {
                    double[] temp3 = this.humidorRHChart.Series["CELL RH"].Points.Where((x, i) => i >= start && i <= end).Select(x => x.YValues[0]).ToArray();
                    ymin3 = temp3.Min();
                    ymax3 = temp3.Max();
                }
                else
                {
                    ymin3 = ymin2;
                    ymax3 = ymax2;
                }

                this.humidorRHChart.ChartAreas["ChartArea1"].AxisY.Minimum = Math.Round(Math.Min(ymin, Math.Min(ymin2, ymin3)))-1;
                this.humidorRHChart.ChartAreas["ChartArea1"].AxisY.Maximum = Math.Round(Math.Max(ymax, Math.Max(ymax2, ymax3)))+1;
                this.humidorRHChart.ChartAreas["ChartArea1"].RecalculateAxesScale();

            }
            catch
            {
                MessageBox.Show("System.Net.WebClient UPDATE ERROR");
            }
        }

        private void humidorDisconnect_Click(object sender, EventArgs e)
        {
            this.humidorConnect.Enabled = true;
            this.humidorDisconnect.Enabled = false;
            this.humidorLastUpdate.Text = "Last Update: DISCONNECTED";
            this.updateTimer.Stop();
            this.humidorConnected = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.updateTimer = new Timer();
            this.updateTimer.Interval = 1000;
            this.updateTimer.Tick += updatetimer_tick;

            this.timestart = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            this.humidorRHChart.Series.Clear();
            this.humidorTChart.Series.Clear();
            this.humidorAHChart.Series.Clear();
            this.humidorPPMChart.Series.Clear();
            
            this.humidorRHChart.Series.Add("INJ RH");
            this.humidorRHChart.Series["INJ RH"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorRHChart.Series["INJ RH"].ChartArea = "ChartArea1";
            this.humidorRHChart.Series["INJ RH"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorTChart.Series.Add("INJ T");
            this.humidorTChart.Series["INJ T"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorTChart.Series["INJ T"].ChartArea = "ChartArea1";
            this.humidorTChart.Series["INJ T"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorAHChart.Series.Add("INJ AH");
            this.humidorAHChart.Series["INJ AH"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorAHChart.Series["INJ AH"].ChartArea = "ChartArea1";
            this.humidorAHChart.Series["INJ AH"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorPPMChart.Series.Add("INJ PPM");
            this.humidorPPMChart.Series["INJ PPM"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorPPMChart.Series["INJ PPM"].ChartArea = "ChartArea1";
            this.humidorPPMChart.Series["INJ PPM"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            
            this.humidorRHChart.Series.Add("EXH RH");
            this.humidorRHChart.Series["EXH RH"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorRHChart.Series["EXH RH"].ChartArea = "ChartArea1";
            this.humidorRHChart.Series["EXH RH"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorTChart.Series.Add("EXH T");
            this.humidorTChart.Series["EXH T"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorTChart.Series["EXH T"].ChartArea = "ChartArea1";
            this.humidorTChart.Series["EXH T"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorAHChart.Series.Add("EXH AH");
            this.humidorAHChart.Series["EXH AH"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorAHChart.Series["EXH AH"].ChartArea = "ChartArea1";
            this.humidorAHChart.Series["EXH AH"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorPPMChart.Series.Add("EXH PPM");
            this.humidorPPMChart.Series["EXH PPM"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorPPMChart.Series["EXH PPM"].ChartArea = "ChartArea1";
            this.humidorPPMChart.Series["EXH PPM"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;


            this.humidorRHChart.Series.Add("CELL RH");
            this.humidorRHChart.Series["CELL RH"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorRHChart.Series["CELL RH"].ChartArea = "ChartArea1";
            this.humidorRHChart.Series["CELL RH"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorTChart.Series.Add("CELL T");
            this.humidorTChart.Series["CELL T"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorTChart.Series["CELL T"].ChartArea = "ChartArea1";
            this.humidorTChart.Series["CELL T"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorAHChart.Series.Add("CELL AH");
            this.humidorAHChart.Series["CELL AH"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorAHChart.Series["CELL AH"].ChartArea = "ChartArea1";
            this.humidorAHChart.Series["CELL AH"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorPPMChart.Series.Add("CELL PPM");
            this.humidorPPMChart.Series["CELL PPM"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            this.humidorPPMChart.Series["CELL PPM"].ChartArea = "ChartArea1";
            this.humidorPPMChart.Series["CELL PPM"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

            this.humidorRHChart.ChartAreas["ChartArea1"].AxisY.Title = "Humidity [% RH]";
            this.humidorTChart.ChartAreas["ChartArea1"].AxisY.Title = "Temperature [° C]";
            this.humidorAHChart.ChartAreas["ChartArea1"].AxisY.Title = "AH [g/L]";
            this.humidorPPMChart.ChartAreas["ChartArea1"].AxisY.Title = "PPMv";
        }

        private void UpdateFormControls()
        {
            this.humidorLastUpdate.Text = String.Concat("Last Update: ", this.parameters_ro.time);
            if (Convert.ToInt32(this.parameters_ro.system.PID.activated) == 1)
            {
                this.humidorPIDActivated.Text = "PID: ENABLED";
            }
            else
            {
                this.humidorPIDActivated.Text = "PID: DISABLED";
            }
            if (Convert.ToInt32(this.parameters_ro.system.activation.cell) == 1)
            {
                this.humidorCellActivated.Text = "Cell: ENABLED";
            }
            else
            {
                this.humidorCellActivated.Text = "Cell: DISABLED";
            }
            if (Convert.ToInt32(this.parameters_ro.system.activation.cell) == 1)
            {
                this.humidorCellActivated.Text = "Cell: ENABLED";
            }
            else
            {
                this.humidorCellActivated.Text = "Cell: DISABLED";
            }
            if (Convert.ToInt32(this.parameters_ro.system.relays.fans) == 1)
            {
                this.FanStatus.Text = "Fans: ENABLED";
            }
            else
            {
                this.FanStatus.Text = "Fans: DISABLED";
            }
            if (Convert.ToInt32(this.parameters_ro.system.relays.atomizer) == 1)
            {
                this.atomizerStatus.Text = "Atomizer: ENABLED";
            }
            else
            {
                this.atomizerStatus.Text = "Atomizer: DISABLED";
            }

            if (textSendAHPID.Text.Length == 0)
            {
                sendToPIDButton.Enabled = false;
            }
            else
            {
                sendToPIDButton.Enabled = true;
            }

            this.humidorPIDSetpoint.Text = String.Concat("PID Setpoint: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.system.PID.setpoint), 2)));
            this.humidorP.Text = String.Concat("Proportional: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.system.PID.proportional), 2)));
            this.humidorI.Text = String.Concat("Integral: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.system.PID.integral), 2)));
            this.humidorD.Text = String.Concat("Derivative: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.system.PID.derivative), 2)));
            this.humidorCTRL.Text = String.Concat("Ctrl: ", this.parameters_ro.system.PID.control_signal);
            this.humidorFlowsWET.Text = String.Concat("WET Flow: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.flows.wet), 2)));
            this.humidorFlowsDRY.Text = String.Concat("DRY Flow: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.flows.dry), 2)));

            this.humidorSensorWETFlow.Text = String.Concat("WET Flow: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.flows.wet), 2)));
            this.humidorSensorDRYFlow.Text = String.Concat("DRY Flow: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.flows.dry), 2)));

            this.humidorSensorINJRH.Text = String.Concat("RH: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.inj.RH), 2)));
            this.humidorSensorINJT.Text = String.Concat("T: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.inj.T), 2)));
            this.humidorSensorINJDP.Text = String.Concat("DP: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.inj.DP), 2)));
            this.humidorSensorINJAH.Text = String.Concat("AH: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.inj.AH), 2)));
            this.humidorSensorINJPPM.Text = String.Concat("PPMv: ", Convert.ToString(Convert.ToInt32(this.parameters_ro.sensors.inj.ppmv)));

            this.humidorSensorEXHRH.Text = String.Concat("RH: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.exh.RH), 2)));
            this.humidorSensorEXHT.Text = String.Concat("T: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.exh.T), 2)));
            this.humidorSensorEXHDP.Text = String.Concat("DP: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.exh.DP), 2)));
            this.humidorSensorEXHAH.Text = String.Concat("AH: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.exh.AH), 2)));
            this.humidorSensorEXHPPM.Text = String.Concat("PPMv: ", Convert.ToString(Convert.ToInt32(this.parameters_ro.sensors.exh.ppmv)));

            this.humidorSensorCELLRH.Text = String.Concat("RH: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.cell.RH), 2)));
            this.humidorSensorCELLT.Text = String.Concat("T: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.cell.T), 2)));
            this.humidorSensorCELLDP.Text = String.Concat("DP: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.cell.DP), 2)));
            this.humidorSensorCELLAH.Text = String.Concat("AH: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.cell.AH), 2)));
            this.humidorSensorCELLPPM.Text = String.Concat("PPMv: ", Convert.ToString(Convert.ToInt32(this.parameters_ro.sensors.cell.ppmv)));

          
            this.webPressure.Text = String.Concat("Pression atm.: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.web.pressure), 2)));
            this.webRH.Text = String.Concat("RH: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.web.RH), 2)));
            this.webT.Text = String.Concat("T: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.web.T), 2)));
            this.webDP.Text = String.Concat("DP: ", Convert.ToString(Math.Round(Convert.ToDouble(this.parameters_ro.sensors.web.DP), 2)));
            this.webLastUpdate.Text = String.Concat("last update: ", Convert.ToString(this.parameters_ro.sensors.web.last_update));

        }

        delegate void scriptPIDToggleCallback();


        private void do_humidorPIDToggle()
        {
            if (this.scriptOutput.InvokeRequired)
            {
                scriptPIDToggleCallback d = new scriptPIDToggleCallback(do_humidorPIDToggle);
                this.Invoke(d);
            } else { 
                try
                {
                    if (Convert.ToInt32(this.parameters_ro.system.PID.activated) == 1)
                    {
                        var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/activated/set/0"));
                        this.humidorWetNew.Enabled = true;
                        this.humidorDryNew.Enabled = true;
                    }
                    else
                    {
                        var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/activated/set/1"));
                        this.humidorWetNew.Enabled = false;
                        this.humidorDryNew.Enabled = false;
                    }
                }
                catch
                {
                    MessageBox.Show("System.Net.WebClient PID TOGGLE ERROR");
                }
            }
        }
        private void humidorPIDToggle_Click(object sender, EventArgs e)
        {
            do_humidorPIDToggle();
        }

        private void humidorServerToUser_Click(object sender, EventArgs e)
        {
            this.humidorWetNew.Text = this.parameters_ro.flows.wet;
            this.humidorDryNew.Text = this.parameters_ro.flows.dry;
            this.humidorPIDNewSetpoint.Text = this.parameters_ro.system.PID.setpoint;
            this.humidorPNew.Text = this.parameters_ro.system.PID.proportional;
            this.humidorINew.Text = this.parameters_ro.system.PID.integral;
            this.humidorDNew.Text = this.parameters_ro.system.PID.derivative;
            this.humidorCTRLNew.Text = this.parameters_ro.system.PID.control_signal;
        }

        private void humidorUserToServer_Click(object sender, EventArgs e)
        {
            try
            {
                this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/setpoint/set/", this.humidorPIDNewSetpoint.Text));
                this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/proportional/set/", this.humidorPNew.Text));
                this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/integral/set/", this.humidorINew.Text));
                this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/derivative/set/", this.humidorDNew.Text));

                this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/control_signal/set/", this.humidorCTRLNew.Text));

                if (Convert.ToInt32(this.parameters_ro.system.PID.activated) == 0)
                {

                    this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/flows/dry/set/", this.humidorDryNew.Text));
                    this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/flows/wet/set/", this.humidorWetNew.Text));
                    // Here we put the WET/DRY Flow UPDATES -> NEED PID TO BE DISABLED!!!!
                }

            }
            catch
            {
                MessageBox.Show("System.Net.WebClient SEND TO SERVER ERROR");
            }
        }

        private void humidorCELLToggle_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(this.parameters_ro.system.activation.cell) == 1)
                {
                    var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/activation/cell/set/0"));
                }
                else
                {
                    var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/activation/cell/set/1"));
                }
            }
            catch
            {
                MessageBox.Show("System.Net.WebClient CELL TOGGLE ERROR");
            }
        }

        private void fanButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(this.parameters_ro.system.relays.fans) == 1)
                {
                    var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/relays/fans/set/0"));
                }
                else
                {
                    var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/relays/fans/set/1"));
                }
            }
            catch
            {
                MessageBox.Show("System.Net.WebClient FAN TOGGLE ERROR");
            }
        }

        private void atomizerButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(this.parameters_ro.system.relays.atomizer) == 1)
                {
                    var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/relays/atomizer/set/0"));
                }
                else
                {
                    var contents = this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/relays/atomizer/set/1"));
                }
            }
            catch
            {
                MessageBox.Show("System.Net.WebClient ATOMIZER TOGGLE ERROR");
            }
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {
            double theta, Pws; 
            double TCrit = 647.096, PCrit = 220640,C1 = -7.85951783, C2 = 1.84408259, C3 = -11.7866497,C4 = 22.6807411,C5 = -15.9618719,C6 = 1.80122502, C = 216.679;
            double T;
            T = Convert.ToDouble(AFMT.Text) + 273.15;

            theta = 1 - T / TCrit;
            Pws = PCrit * Math.Exp(TCrit / T * (C1 * theta + C2 * Math.Pow(theta,1.5) + C3 * Math.Pow(theta, 3) + C4 * Math.Pow(theta, 3.5) + C5 * Math.Pow(theta, 4) + C6 * Math.Pow(theta, 7.5)));

            double Pw = Pws * Convert.ToDouble(targetRH.Text) / 100.0;
            double AH = C * Pw / T;
            textNeededAH.Text = Convert.ToString(AH);
            textSendAHPID.Text = Convert.ToString(AH * Convert.ToDouble(textCorrFactor.Text));



        }

        private void sendToPIDButton_Click(object sender, EventArgs e)
        {
            this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/control_signal/set/sensors.inj.AH"));
            this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/setpoint/set/", this.textSendAHPID.Text));
            this.humidorConn.DownloadString(string.Concat("http://", humidorIP.Text, ":", humidorPort.Text, "/system/PID/activated/set/1"));
            this.humidorWetNew.Enabled = false;
            this.humidorDryNew.Enabled = false;

        }

        private void scriptRunButton_Click(object sender, EventArgs e)
        {
            
            object result = scriptInterpreter.RequestScriptExecution(scriptInput.Text);
        }

        private void scriptStopButton_Click(object sender, EventArgs e)
        {
            scriptInterpreter.Stop();
        }

        private void scriptOutputClear_Click(object sender, EventArgs e)
        {
            scriptOutput.Clear();
        }

        delegate void scriptParameterSetCallback(object target, object value);

        private void do_humidorParameterSet(object target, object value)
        {
            if (this.scriptOutput.InvokeRequired)
            {
                scriptParameterSetCallback d = new scriptParameterSetCallback(do_humidorParameterSet);
                this.Invoke(d, new object[] { target }, new object[] { value });
            }
            else {
                try
                {
                    MessageBox.Show(string.Concat("Setting ", target.ToString(), " to ", value.ToString()));
                }
                catch
                {
                    MessageBox.Show("System.Net.WebClient SET PARAM ERROR");
                }
            }
        }

        delegate object scriptParameterGetCallback(object target);

        private object do_humidorParameterGet(object target)
        {
            if (this.scriptOutput.InvokeRequired)
            {
                scriptParameterGetCallback d = new scriptParameterGetCallback(do_humidorParameterGet);
                this.Invoke(d, new object[] { target });
            }
            else {
                try
                {
                    MessageBox.Show(string.Concat("Getting ", target.ToString()));
                    return target;
                }
                catch
                {
                    MessageBox.Show("System.Net.WebClient SET PARAM ERROR");
                    return "ERROR";
                }
            }
            return "ERROR";
        }
    }
}
