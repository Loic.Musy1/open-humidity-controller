﻿namespace HumidorControlPro
{
    partial class Form1
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend11 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend12 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.humidorIP = new System.Windows.Forms.TextBox();
            this.humidorIPLabel = new System.Windows.Forms.Label();
            this.humidorPortLabel = new System.Windows.Forms.Label();
            this.humidorPort = new System.Windows.Forms.TextBox();
            this.humidorConnect = new System.Windows.Forms.Button();
            this.humidorDisconnect = new System.Windows.Forms.Button();
            this.debugTextBox = new System.Windows.Forms.TextBox();
            this.humidorLastUpdate = new System.Windows.Forms.Label();
            this.humidorSensorWETFlow = new System.Windows.Forms.Label();
            this.humidorSensorDRYFlow = new System.Windows.Forms.Label();
            this.humidorSensorINJRH = new System.Windows.Forms.Label();
            this.humidorSensorINJT = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.humidorSensorCELLPPM = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.humidorSensorCELLAH = new System.Windows.Forms.Label();
            this.humidorSensorCELLRH = new System.Windows.Forms.Label();
            this.humidorSensorCELLDP = new System.Windows.Forms.Label();
            this.humidorSensorCELLT = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.humidorSensorEXHPPM = new System.Windows.Forms.Label();
            this.humidorSensorEXHDP = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.humidorSensorEXHRH = new System.Windows.Forms.Label();
            this.humidorSensorEXHT = new System.Windows.Forms.Label();
            this.humidorSensorEXHAH = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.humidorSensorINJPPM = new System.Windows.Forms.Label();
            this.humidorSensorINJAH = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.humidorSensorINJDP = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.humidorCTRL = new System.Windows.Forms.Label();
            this.humidorD = new System.Windows.Forms.Label();
            this.humidorI = new System.Windows.Forms.Label();
            this.humidorP = new System.Windows.Forms.Label();
            this.humidorPIDSetpoint = new System.Windows.Forms.Label();
            this.humidorFlowsWET = new System.Windows.Forms.Label();
            this.humidorFlowsDRY = new System.Windows.Forms.Label();
            this.humidorPIDActivated = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.humidorPIDToggle = new System.Windows.Forms.Button();
            this.humidorPIDNewSetpoint = new System.Windows.Forms.TextBox();
            this.humidorUserToServer = new System.Windows.Forms.Button();
            this.humidorServerToUser = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.humidorRHChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.tabcontrol = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.humidorTChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.humidorAHChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.humidorPPMChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.trackBar4 = new System.Windows.Forms.TrackBar();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.scriptRunButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.scriptOutput = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.scriptInput = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.proportional_label = new System.Windows.Forms.Label();
            this.integral_label = new System.Windows.Forms.Label();
            this.derivative_label = new System.Windows.Forms.Label();
            this.humidorPNew = new System.Windows.Forms.TextBox();
            this.humidorINew = new System.Windows.Forms.TextBox();
            this.humidorDNew = new System.Windows.Forms.TextBox();
            this.humidorCELLToggle = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.humidorWetNew = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.humidorDryNew = new System.Windows.Forms.TextBox();
            this.humidorCTRLNew = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.textSendAHPID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.targetRH = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textCorrFactor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textNeededAH = new System.Windows.Forms.TextBox();
            this.AFMT = new System.Windows.Forms.TextBox();
            this.sendToPIDButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.atomizerStatus = new System.Windows.Forms.Label();
            this.fanButton = new System.Windows.Forms.Button();
            this.atomizerButton = new System.Windows.Forms.Button();
            this.FanStatus = new System.Windows.Forms.Label();
            this.humidorCellActivated = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.webPressure = new System.Windows.Forms.Label();
            this.webRH = new System.Windows.Forms.Label();
            this.webDP = new System.Windows.Forms.Label();
            this.webT = new System.Windows.Forms.Label();
            this.webLastUpdate = new System.Windows.Forms.Label();
            this.scriptStopButton = new System.Windows.Forms.Button();
            this.scriptOutputClear = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidorRHChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.tabcontrol.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidorTChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidorAHChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidorPPMChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).BeginInit();
            this.tabPage10.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // humidorIP
            // 
            this.humidorIP.Location = new System.Drawing.Point(121, 14);
            this.humidorIP.Name = "humidorIP";
            this.humidorIP.Size = new System.Drawing.Size(153, 26);
            this.humidorIP.TabIndex = 0;
            this.humidorIP.Text = "10.10.10.10";
            // 
            // humidorIPLabel
            // 
            this.humidorIPLabel.AutoSize = true;
            this.humidorIPLabel.Location = new System.Drawing.Point(12, 20);
            this.humidorIPLabel.Name = "humidorIPLabel";
            this.humidorIPLabel.Size = new System.Drawing.Size(91, 20);
            this.humidorIPLabel.TabIndex = 1;
            this.humidorIPLabel.Text = "IP Address:";
            // 
            // humidorPortLabel
            // 
            this.humidorPortLabel.AutoSize = true;
            this.humidorPortLabel.Location = new System.Drawing.Point(12, 52);
            this.humidorPortLabel.Name = "humidorPortLabel";
            this.humidorPortLabel.Size = new System.Drawing.Size(100, 20);
            this.humidorPortLabel.TabIndex = 2;
            this.humidorPortLabel.Text = "Port number:";
            // 
            // humidorPort
            // 
            this.humidorPort.Location = new System.Drawing.Point(121, 46);
            this.humidorPort.Name = "humidorPort";
            this.humidorPort.Size = new System.Drawing.Size(153, 26);
            this.humidorPort.TabIndex = 3;
            this.humidorPort.Text = "9000";
            // 
            // humidorConnect
            // 
            this.humidorConnect.Location = new System.Drawing.Point(295, 20);
            this.humidorConnect.Name = "humidorConnect";
            this.humidorConnect.Size = new System.Drawing.Size(108, 52);
            this.humidorConnect.TabIndex = 4;
            this.humidorConnect.Text = "Connect";
            this.humidorConnect.UseVisualStyleBackColor = true;
            this.humidorConnect.Click += new System.EventHandler(this.humidorConnect_Click);
            // 
            // humidorDisconnect
            // 
            this.humidorDisconnect.Enabled = false;
            this.humidorDisconnect.Location = new System.Drawing.Point(418, 20);
            this.humidorDisconnect.Name = "humidorDisconnect";
            this.humidorDisconnect.Size = new System.Drawing.Size(108, 52);
            this.humidorDisconnect.TabIndex = 5;
            this.humidorDisconnect.Text = "Disconnect";
            this.humidorDisconnect.UseVisualStyleBackColor = true;
            this.humidorDisconnect.Click += new System.EventHandler(this.humidorDisconnect_Click);
            // 
            // debugTextBox
            // 
            this.debugTextBox.Location = new System.Drawing.Point(8, 1128);
            this.debugTextBox.Multiline = true;
            this.debugTextBox.Name = "debugTextBox";
            this.debugTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.debugTextBox.Size = new System.Drawing.Size(1457, 114);
            this.debugTextBox.TabIndex = 6;
            // 
            // humidorLastUpdate
            // 
            this.humidorLastUpdate.AutoSize = true;
            this.humidorLastUpdate.Location = new System.Drawing.Point(767, 14);
            this.humidorLastUpdate.Name = "humidorLastUpdate";
            this.humidorLastUpdate.Size = new System.Drawing.Size(232, 20);
            this.humidorLastUpdate.TabIndex = 8;
            this.humidorLastUpdate.Text = "Last Update: DISCONNECTED";
            // 
            // humidorSensorWETFlow
            // 
            this.humidorSensorWETFlow.AutoSize = true;
            this.humidorSensorWETFlow.Location = new System.Drawing.Point(19, 15);
            this.humidorSensorWETFlow.Name = "humidorSensorWETFlow";
            this.humidorSensorWETFlow.Size = new System.Drawing.Size(94, 20);
            this.humidorSensorWETFlow.TabIndex = 9;
            this.humidorSensorWETFlow.Text = "WET Flow: -";
            // 
            // humidorSensorDRYFlow
            // 
            this.humidorSensorDRYFlow.AutoSize = true;
            this.humidorSensorDRYFlow.Location = new System.Drawing.Point(19, 35);
            this.humidorSensorDRYFlow.Name = "humidorSensorDRYFlow";
            this.humidorSensorDRYFlow.Size = new System.Drawing.Size(94, 20);
            this.humidorSensorDRYFlow.TabIndex = 10;
            this.humidorSensorDRYFlow.Text = "DRY Flow: -";
            // 
            // humidorSensorINJRH
            // 
            this.humidorSensorINJRH.AutoSize = true;
            this.humidorSensorINJRH.Location = new System.Drawing.Point(18, 31);
            this.humidorSensorINJRH.Name = "humidorSensorINJRH";
            this.humidorSensorINJRH.Size = new System.Drawing.Size(46, 20);
            this.humidorSensorINJRH.TabIndex = 11;
            this.humidorSensorINJRH.Text = "RH: -";
            // 
            // humidorSensorINJT
            // 
            this.humidorSensorINJT.AutoSize = true;
            this.humidorSensorINJT.Location = new System.Drawing.Point(31, 54);
            this.humidorSensorINJT.Name = "humidorSensorINJT";
            this.humidorSensorINJT.Size = new System.Drawing.Size(31, 20);
            this.humidorSensorINJT.TabIndex = 12;
            this.humidorSensorINJT.Text = "T: -";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.humidorSensorWETFlow);
            this.panel1.Controls.Add(this.humidorSensorDRYFlow);
            this.panel1.Location = new System.Drawing.Point(6, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(408, 223);
            this.panel1.TabIndex = 13;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.humidorSensorCELLPPM);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.humidorSensorCELLAH);
            this.panel5.Controls.Add(this.humidorSensorCELLRH);
            this.panel5.Controls.Add(this.humidorSensorCELLDP);
            this.panel5.Controls.Add(this.humidorSensorCELLT);
            this.panel5.Location = new System.Drawing.Point(282, 64);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(119, 156);
            this.panel5.TabIndex = 24;
            // 
            // humidorSensorCELLPPM
            // 
            this.humidorSensorCELLPPM.AutoSize = true;
            this.humidorSensorCELLPPM.Location = new System.Drawing.Point(1, 123);
            this.humidorSensorCELLPPM.Name = "humidorSensorCELLPPM";
            this.humidorSensorCELLPPM.Size = new System.Drawing.Size(62, 20);
            this.humidorSensorCELLPPM.TabIndex = 26;
            this.humidorSensorCELLPPM.Text = "PPMv: -";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 20);
            this.label12.TabIndex = 25;
            this.label12.Text = "CELL";
            // 
            // humidorSensorCELLAH
            // 
            this.humidorSensorCELLAH.AutoSize = true;
            this.humidorSensorCELLAH.Location = new System.Drawing.Point(18, 100);
            this.humidorSensorCELLAH.Name = "humidorSensorCELLAH";
            this.humidorSensorCELLAH.Size = new System.Drawing.Size(45, 20);
            this.humidorSensorCELLAH.TabIndex = 22;
            this.humidorSensorCELLAH.Text = "AH: -";
            // 
            // humidorSensorCELLRH
            // 
            this.humidorSensorCELLRH.AutoSize = true;
            this.humidorSensorCELLRH.Location = new System.Drawing.Point(18, 31);
            this.humidorSensorCELLRH.Name = "humidorSensorCELLRH";
            this.humidorSensorCELLRH.Size = new System.Drawing.Size(46, 20);
            this.humidorSensorCELLRH.TabIndex = 15;
            this.humidorSensorCELLRH.Text = "RH: -";
            // 
            // humidorSensorCELLDP
            // 
            this.humidorSensorCELLDP.AutoSize = true;
            this.humidorSensorCELLDP.Location = new System.Drawing.Point(18, 77);
            this.humidorSensorCELLDP.Name = "humidorSensorCELLDP";
            this.humidorSensorCELLDP.Size = new System.Drawing.Size(44, 20);
            this.humidorSensorCELLDP.TabIndex = 21;
            this.humidorSensorCELLDP.Text = "DP: -";
            // 
            // humidorSensorCELLT
            // 
            this.humidorSensorCELLT.AutoSize = true;
            this.humidorSensorCELLT.Location = new System.Drawing.Point(31, 54);
            this.humidorSensorCELLT.Name = "humidorSensorCELLT";
            this.humidorSensorCELLT.Size = new System.Drawing.Size(31, 20);
            this.humidorSensorCELLT.TabIndex = 16;
            this.humidorSensorCELLT.Text = "T: -";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.humidorSensorEXHPPM);
            this.panel4.Controls.Add(this.humidorSensorEXHDP);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.humidorSensorEXHRH);
            this.panel4.Controls.Add(this.humidorSensorEXHT);
            this.panel4.Controls.Add(this.humidorSensorEXHAH);
            this.panel4.Location = new System.Drawing.Point(144, 64);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(128, 156);
            this.panel4.TabIndex = 24;
            // 
            // humidorSensorEXHPPM
            // 
            this.humidorSensorEXHPPM.AutoSize = true;
            this.humidorSensorEXHPPM.Location = new System.Drawing.Point(1, 123);
            this.humidorSensorEXHPPM.Name = "humidorSensorEXHPPM";
            this.humidorSensorEXHPPM.Size = new System.Drawing.Size(62, 20);
            this.humidorSensorEXHPPM.TabIndex = 25;
            this.humidorSensorEXHPPM.Text = "PPMv: -";
            // 
            // humidorSensorEXHDP
            // 
            this.humidorSensorEXHDP.AutoSize = true;
            this.humidorSensorEXHDP.Location = new System.Drawing.Point(18, 77);
            this.humidorSensorEXHDP.Name = "humidorSensorEXHDP";
            this.humidorSensorEXHDP.Size = new System.Drawing.Size(44, 20);
            this.humidorSensorEXHDP.TabIndex = 19;
            this.humidorSensorEXHDP.Text = "DP: -";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 20);
            this.label11.TabIndex = 24;
            this.label11.Text = "EXHAUST";
            // 
            // humidorSensorEXHRH
            // 
            this.humidorSensorEXHRH.AutoSize = true;
            this.humidorSensorEXHRH.Location = new System.Drawing.Point(18, 31);
            this.humidorSensorEXHRH.Name = "humidorSensorEXHRH";
            this.humidorSensorEXHRH.Size = new System.Drawing.Size(46, 20);
            this.humidorSensorEXHRH.TabIndex = 13;
            this.humidorSensorEXHRH.Text = "RH: -";
            // 
            // humidorSensorEXHT
            // 
            this.humidorSensorEXHT.AutoSize = true;
            this.humidorSensorEXHT.Location = new System.Drawing.Point(31, 54);
            this.humidorSensorEXHT.Name = "humidorSensorEXHT";
            this.humidorSensorEXHT.Size = new System.Drawing.Size(31, 20);
            this.humidorSensorEXHT.TabIndex = 14;
            this.humidorSensorEXHT.Text = "T: -";
            // 
            // humidorSensorEXHAH
            // 
            this.humidorSensorEXHAH.AutoSize = true;
            this.humidorSensorEXHAH.Location = new System.Drawing.Point(18, 100);
            this.humidorSensorEXHAH.Name = "humidorSensorEXHAH";
            this.humidorSensorEXHAH.Size = new System.Drawing.Size(45, 20);
            this.humidorSensorEXHAH.TabIndex = 20;
            this.humidorSensorEXHAH.Text = "AH: -";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.humidorSensorINJPPM);
            this.panel3.Controls.Add(this.humidorSensorINJT);
            this.panel3.Controls.Add(this.humidorSensorINJAH);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.humidorSensorINJRH);
            this.panel3.Controls.Add(this.humidorSensorINJDP);
            this.panel3.Location = new System.Drawing.Point(9, 64);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(127, 156);
            this.panel3.TabIndex = 23;
            // 
            // humidorSensorINJPPM
            // 
            this.humidorSensorINJPPM.AutoSize = true;
            this.humidorSensorINJPPM.Location = new System.Drawing.Point(1, 123);
            this.humidorSensorINJPPM.Name = "humidorSensorINJPPM";
            this.humidorSensorINJPPM.Size = new System.Drawing.Size(62, 20);
            this.humidorSensorINJPPM.TabIndex = 24;
            this.humidorSensorINJPPM.Text = "PPMv: -";
            // 
            // humidorSensorINJAH
            // 
            this.humidorSensorINJAH.AutoSize = true;
            this.humidorSensorINJAH.Location = new System.Drawing.Point(18, 100);
            this.humidorSensorINJAH.Name = "humidorSensorINJAH";
            this.humidorSensorINJAH.Size = new System.Drawing.Size(45, 20);
            this.humidorSensorINJAH.TabIndex = 18;
            this.humidorSensorINJAH.Text = "AH: -";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(30, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 20);
            this.label10.TabIndex = 23;
            this.label10.Text = "INJECT";
            // 
            // humidorSensorINJDP
            // 
            this.humidorSensorINJDP.AutoSize = true;
            this.humidorSensorINJDP.Location = new System.Drawing.Point(18, 77);
            this.humidorSensorINJDP.Name = "humidorSensorINJDP";
            this.humidorSensorINJDP.Size = new System.Drawing.Size(44, 20);
            this.humidorSensorINJDP.TabIndex = 17;
            this.humidorSensorINJDP.Text = "DP: -";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.humidorCTRL);
            this.panel2.Controls.Add(this.humidorD);
            this.panel2.Controls.Add(this.humidorI);
            this.panel2.Controls.Add(this.humidorP);
            this.panel2.Controls.Add(this.humidorPIDSetpoint);
            this.panel2.Controls.Add(this.humidorFlowsWET);
            this.panel2.Controls.Add(this.humidorFlowsDRY);
            this.panel2.Location = new System.Drawing.Point(445, 130);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 220);
            this.panel2.TabIndex = 14;
            // 
            // humidorCTRL
            // 
            this.humidorCTRL.AutoSize = true;
            this.humidorCTRL.Location = new System.Drawing.Point(41, 187);
            this.humidorCTRL.Name = "humidorCTRL";
            this.humidorCTRL.Size = new System.Drawing.Size(46, 20);
            this.humidorCTRL.TabIndex = 27;
            this.humidorCTRL.Text = "Ctrl: -";
            // 
            // humidorD
            // 
            this.humidorD.AutoSize = true;
            this.humidorD.Location = new System.Drawing.Point(41, 159);
            this.humidorD.Name = "humidorD";
            this.humidorD.Size = new System.Drawing.Size(91, 20);
            this.humidorD.TabIndex = 26;
            this.humidorD.Text = "Derivative: -";
            // 
            // humidorI
            // 
            this.humidorI.AutoSize = true;
            this.humidorI.Location = new System.Drawing.Point(56, 136);
            this.humidorI.Name = "humidorI";
            this.humidorI.Size = new System.Drawing.Size(76, 20);
            this.humidorI.TabIndex = 25;
            this.humidorI.Text = "Integral: -";
            // 
            // humidorP
            // 
            this.humidorP.AutoSize = true;
            this.humidorP.Location = new System.Drawing.Point(25, 113);
            this.humidorP.Name = "humidorP";
            this.humidorP.Size = new System.Drawing.Size(107, 20);
            this.humidorP.TabIndex = 24;
            this.humidorP.Text = "Proportional: -";
            // 
            // humidorPIDSetpoint
            // 
            this.humidorPIDSetpoint.AutoSize = true;
            this.humidorPIDSetpoint.Location = new System.Drawing.Point(19, 90);
            this.humidorPIDSetpoint.Name = "humidorPIDSetpoint";
            this.humidorPIDSetpoint.Size = new System.Drawing.Size(113, 20);
            this.humidorPIDSetpoint.TabIndex = 12;
            this.humidorPIDSetpoint.Text = "PID Setpoint: -";
            // 
            // humidorFlowsWET
            // 
            this.humidorFlowsWET.AutoSize = true;
            this.humidorFlowsWET.Location = new System.Drawing.Point(19, 15);
            this.humidorFlowsWET.Name = "humidorFlowsWET";
            this.humidorFlowsWET.Size = new System.Drawing.Size(94, 20);
            this.humidorFlowsWET.TabIndex = 9;
            this.humidorFlowsWET.Text = "WET Flow: -";
            // 
            // humidorFlowsDRY
            // 
            this.humidorFlowsDRY.AutoSize = true;
            this.humidorFlowsDRY.Location = new System.Drawing.Point(19, 44);
            this.humidorFlowsDRY.Name = "humidorFlowsDRY";
            this.humidorFlowsDRY.Size = new System.Drawing.Size(94, 20);
            this.humidorFlowsDRY.TabIndex = 10;
            this.humidorFlowsDRY.Text = "DRY Flow: -";
            // 
            // humidorPIDActivated
            // 
            this.humidorPIDActivated.AutoSize = true;
            this.humidorPIDActivated.Location = new System.Drawing.Point(129, 12);
            this.humidorPIDActivated.Name = "humidorPIDActivated";
            this.humidorPIDActivated.Size = new System.Drawing.Size(49, 20);
            this.humidorPIDActivated.TabIndex = 11;
            this.humidorPIDActivated.Text = "PID: -";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(464, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 20);
            this.label6.TabIndex = 15;
            this.label6.Text = "Control Parameters";
            // 
            // humidorPIDToggle
            // 
            this.humidorPIDToggle.Location = new System.Drawing.Point(6, 6);
            this.humidorPIDToggle.Name = "humidorPIDToggle";
            this.humidorPIDToggle.Size = new System.Drawing.Size(117, 33);
            this.humidorPIDToggle.TabIndex = 16;
            this.humidorPIDToggle.Text = "PID ON/OFF";
            this.humidorPIDToggle.UseVisualStyleBackColor = true;
            this.humidorPIDToggle.Click += new System.EventHandler(this.humidorPIDToggle_Click);
            // 
            // humidorPIDNewSetpoint
            // 
            this.humidorPIDNewSetpoint.Location = new System.Drawing.Point(124, 120);
            this.humidorPIDNewSetpoint.Name = "humidorPIDNewSetpoint";
            this.humidorPIDNewSetpoint.Size = new System.Drawing.Size(121, 26);
            this.humidorPIDNewSetpoint.TabIndex = 17;
            this.humidorPIDNewSetpoint.Text = "0";
            // 
            // humidorUserToServer
            // 
            this.humidorUserToServer.Location = new System.Drawing.Point(651, 233);
            this.humidorUserToServer.Name = "humidorUserToServer";
            this.humidorUserToServer.Size = new System.Drawing.Size(93, 29);
            this.humidorUserToServer.TabIndex = 18;
            this.humidorUserToServer.Text = "<-";
            this.humidorUserToServer.UseVisualStyleBackColor = true;
            this.humidorUserToServer.Click += new System.EventHandler(this.humidorUserToServer_Click);
            // 
            // humidorServerToUser
            // 
            this.humidorServerToUser.Location = new System.Drawing.Point(651, 174);
            this.humidorServerToUser.Name = "humidorServerToUser";
            this.humidorServerToUser.Size = new System.Drawing.Size(93, 36);
            this.humidorServerToUser.TabIndex = 19;
            this.humidorServerToUser.Text = "->";
            this.humidorServerToUser.UseVisualStyleBackColor = true;
            this.humidorServerToUser.Click += new System.EventHandler(this.humidorServerToUser_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "PID Setpoint:";
            // 
            // humidorRHChart
            // 
            chartArea9.Name = "ChartArea1";
            this.humidorRHChart.ChartAreas.Add(chartArea9);
            legend9.Name = "Legend1";
            this.humidorRHChart.Legends.Add(legend9);
            this.humidorRHChart.Location = new System.Drawing.Point(6, 6);
            this.humidorRHChart.Name = "humidorRHChart";
            series9.ChartArea = "ChartArea1";
            series9.Legend = "Legend1";
            series9.Name = "Series1";
            this.humidorRHChart.Series.Add(series9);
            this.humidorRHChart.Size = new System.Drawing.Size(1437, 626);
            this.humidorRHChart.TabIndex = 20;
            this.humidorRHChart.Text = "chart1";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(6, 638);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(1437, 69);
            this.trackBar1.TabIndex = 21;
            // 
            // tabcontrol
            // 
            this.tabcontrol.Controls.Add(this.tabPage1);
            this.tabcontrol.Controls.Add(this.tabPage2);
            this.tabcontrol.Controls.Add(this.tabPage3);
            this.tabcontrol.Controls.Add(this.tabPage4);
            this.tabcontrol.Controls.Add(this.tabPage10);
            this.tabcontrol.Location = new System.Drawing.Point(8, 364);
            this.tabcontrol.Name = "tabcontrol";
            this.tabcontrol.SelectedIndex = 0;
            this.tabcontrol.Size = new System.Drawing.Size(1457, 755);
            this.tabcontrol.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.humidorRHChart);
            this.tabPage1.Controls.Add(this.trackBar1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1449, 722);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Humidity";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.humidorTChart);
            this.tabPage2.Controls.Add(this.trackBar2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1449, 722);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Temperature";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // humidorTChart
            // 
            chartArea10.Name = "ChartArea1";
            this.humidorTChart.ChartAreas.Add(chartArea10);
            legend10.Name = "Legend1";
            this.humidorTChart.Legends.Add(legend10);
            this.humidorTChart.Location = new System.Drawing.Point(6, 6);
            this.humidorTChart.Name = "humidorTChart";
            series10.ChartArea = "ChartArea1";
            series10.Legend = "Legend1";
            series10.Name = "Series1";
            this.humidorTChart.Series.Add(series10);
            this.humidorTChart.Size = new System.Drawing.Size(1437, 626);
            this.humidorTChart.TabIndex = 22;
            this.humidorTChart.Text = "chart1";
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(6, 638);
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(1437, 69);
            this.trackBar2.TabIndex = 23;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.humidorAHChart);
            this.tabPage3.Controls.Add(this.trackBar3);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1449, 722);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Abs. Humidity";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // humidorAHChart
            // 
            chartArea11.Name = "ChartArea1";
            this.humidorAHChart.ChartAreas.Add(chartArea11);
            legend11.Name = "Legend1";
            this.humidorAHChart.Legends.Add(legend11);
            this.humidorAHChart.Location = new System.Drawing.Point(6, 6);
            this.humidorAHChart.Name = "humidorAHChart";
            series11.ChartArea = "ChartArea1";
            series11.Legend = "Legend1";
            series11.Name = "Series1";
            this.humidorAHChart.Series.Add(series11);
            this.humidorAHChart.Size = new System.Drawing.Size(1437, 626);
            this.humidorAHChart.TabIndex = 24;
            this.humidorAHChart.Text = "chart1";
            // 
            // trackBar3
            // 
            this.trackBar3.Location = new System.Drawing.Point(6, 638);
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(1437, 69);
            this.trackBar3.TabIndex = 25;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.humidorPPMChart);
            this.tabPage4.Controls.Add(this.trackBar4);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1449, 722);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "PPMv";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // humidorPPMChart
            // 
            chartArea12.Name = "ChartArea1";
            this.humidorPPMChart.ChartAreas.Add(chartArea12);
            legend12.Name = "Legend1";
            this.humidorPPMChart.Legends.Add(legend12);
            this.humidorPPMChart.Location = new System.Drawing.Point(6, 6);
            this.humidorPPMChart.Name = "humidorPPMChart";
            series12.ChartArea = "ChartArea1";
            series12.Legend = "Legend1";
            series12.Name = "Series1";
            this.humidorPPMChart.Series.Add(series12);
            this.humidorPPMChart.Size = new System.Drawing.Size(1437, 626);
            this.humidorPPMChart.TabIndex = 26;
            this.humidorPPMChart.Text = "chart1";
            // 
            // trackBar4
            // 
            this.trackBar4.Location = new System.Drawing.Point(6, 638);
            this.trackBar4.Name = "trackBar4";
            this.trackBar4.Size = new System.Drawing.Size(1437, 69);
            this.trackBar4.TabIndex = 27;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.scriptOutputClear);
            this.tabPage10.Controls.Add(this.scriptStopButton);
            this.tabPage10.Controls.Add(this.scriptRunButton);
            this.tabPage10.Controls.Add(this.button3);
            this.tabPage10.Controls.Add(this.button2);
            this.tabPage10.Controls.Add(this.scriptOutput);
            this.tabPage10.Controls.Add(this.label17);
            this.tabPage10.Controls.Add(this.scriptInput);
            this.tabPage10.Controls.Add(this.label16);
            this.tabPage10.Location = new System.Drawing.Point(4, 29);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(1449, 722);
            this.tabPage10.TabIndex = 4;
            this.tabPage10.Text = "Scripting";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // scriptRunButton
            // 
            this.scriptRunButton.Location = new System.Drawing.Point(375, 354);
            this.scriptRunButton.Name = "scriptRunButton";
            this.scriptRunButton.Size = new System.Drawing.Size(139, 45);
            this.scriptRunButton.TabIndex = 38;
            this.scriptRunButton.Text = "Run script";
            this.scriptRunButton.UseVisualStyleBackColor = true;
            this.scriptRunButton.Click += new System.EventHandler(this.scriptRunButton_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(212, 354);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 45);
            this.button3.TabIndex = 37;
            this.button3.Text = "Save to file";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(44, 354);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 45);
            this.button2.TabIndex = 36;
            this.button2.Text = "Load from file";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // scriptOutput
            // 
            this.scriptOutput.Location = new System.Drawing.Point(29, 211);
            this.scriptOutput.Multiline = true;
            this.scriptOutput.Name = "scriptOutput";
            this.scriptOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.scriptOutput.Size = new System.Drawing.Size(1118, 114);
            this.scriptOutput.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(23, 188);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 20);
            this.label17.TabIndex = 34;
            this.label17.Text = "Output";
            // 
            // scriptInput
            // 
            this.scriptInput.Location = new System.Drawing.Point(21, 44);
            this.scriptInput.Multiline = true;
            this.scriptInput.Name = "scriptInput";
            this.scriptInput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.scriptInput.Size = new System.Drawing.Size(1118, 114);
            this.scriptInput.TabIndex = 33;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "Script";
            // 
            // proportional_label
            // 
            this.proportional_label.AutoSize = true;
            this.proportional_label.Location = new System.Drawing.Point(20, 156);
            this.proportional_label.Name = "proportional_label";
            this.proportional_label.Size = new System.Drawing.Size(98, 20);
            this.proportional_label.TabIndex = 23;
            this.proportional_label.Text = "Proportional:";
            // 
            // integral_label
            // 
            this.integral_label.AutoSize = true;
            this.integral_label.Location = new System.Drawing.Point(51, 192);
            this.integral_label.Name = "integral_label";
            this.integral_label.Size = new System.Drawing.Size(67, 20);
            this.integral_label.TabIndex = 24;
            this.integral_label.Text = "Integral:";
            // 
            // derivative_label
            // 
            this.derivative_label.AutoSize = true;
            this.derivative_label.Location = new System.Drawing.Point(36, 228);
            this.derivative_label.Name = "derivative_label";
            this.derivative_label.Size = new System.Drawing.Size(82, 20);
            this.derivative_label.TabIndex = 25;
            this.derivative_label.Text = "Derivative:";
            // 
            // humidorPNew
            // 
            this.humidorPNew.Location = new System.Drawing.Point(124, 156);
            this.humidorPNew.Name = "humidorPNew";
            this.humidorPNew.Size = new System.Drawing.Size(121, 26);
            this.humidorPNew.TabIndex = 26;
            this.humidorPNew.Text = "8";
            // 
            // humidorINew
            // 
            this.humidorINew.Location = new System.Drawing.Point(124, 192);
            this.humidorINew.Name = "humidorINew";
            this.humidorINew.Size = new System.Drawing.Size(121, 26);
            this.humidorINew.TabIndex = 27;
            this.humidorINew.Text = "1";
            // 
            // humidorDNew
            // 
            this.humidorDNew.Location = new System.Drawing.Point(124, 228);
            this.humidorDNew.Name = "humidorDNew";
            this.humidorDNew.Size = new System.Drawing.Size(121, 26);
            this.humidorDNew.TabIndex = 28;
            this.humidorDNew.Text = "0";
            // 
            // humidorCELLToggle
            // 
            this.humidorCELLToggle.Location = new System.Drawing.Point(17, 14);
            this.humidorCELLToggle.Name = "humidorCELLToggle";
            this.humidorCELLToggle.Size = new System.Drawing.Size(129, 33);
            this.humidorCELLToggle.TabIndex = 29;
            this.humidorCELLToggle.Text = "CELL ON/OFF";
            this.humidorCELLToggle.UseVisualStyleBackColor = true;
            this.humidorCELLToggle.Click += new System.EventHandler(this.humidorCELLToggle_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Location = new System.Drawing.Point(750, 46);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(360, 344);
            this.tabControl1.TabIndex = 30;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Transparent;
            this.tabPage5.Controls.Add(this.humidorWetNew);
            this.tabPage5.Controls.Add(this.label14);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.humidorDryNew);
            this.tabPage5.Controls.Add(this.humidorCTRLNew);
            this.tabPage5.Controls.Add(this.humidorPIDActivated);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Controls.Add(this.humidorPIDToggle);
            this.tabPage5.Controls.Add(this.humidorPIDNewSetpoint);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Controls.Add(this.proportional_label);
            this.tabPage5.Controls.Add(this.humidorDNew);
            this.tabPage5.Controls.Add(this.integral_label);
            this.tabPage5.Controls.Add(this.humidorINew);
            this.tabPage5.Controls.Add(this.derivative_label);
            this.tabPage5.Controls.Add(this.humidorPNew);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(352, 311);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "PID";
            // 
            // humidorWetNew
            // 
            this.humidorWetNew.Location = new System.Drawing.Point(124, 49);
            this.humidorWetNew.Name = "humidorWetNew";
            this.humidorWetNew.Size = new System.Drawing.Size(121, 26);
            this.humidorWetNew.TabIndex = 34;
            this.humidorWetNew.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(37, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 20);
            this.label14.TabIndex = 33;
            this.label14.Text = "Wet Flow:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(34, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 20);
            this.label15.TabIndex = 35;
            this.label15.Text = "Dry Flow:";
            // 
            // humidorDryNew
            // 
            this.humidorDryNew.Location = new System.Drawing.Point(124, 85);
            this.humidorDryNew.Name = "humidorDryNew";
            this.humidorDryNew.Size = new System.Drawing.Size(121, 26);
            this.humidorDryNew.TabIndex = 36;
            this.humidorDryNew.Text = "0";
            // 
            // humidorCTRLNew
            // 
            this.humidorCTRLNew.Location = new System.Drawing.Point(124, 264);
            this.humidorCTRLNew.Name = "humidorCTRLNew";
            this.humidorCTRLNew.Size = new System.Drawing.Size(121, 26);
            this.humidorCTRLNew.TabIndex = 32;
            this.humidorCTRLNew.Text = "sensors.inj.RH";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 20);
            this.label3.TabIndex = 31;
            this.label3.Text = "Ctrl Signal:";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.Transparent;
            this.tabPage6.Controls.Add(this.label13);
            this.tabPage6.Controls.Add(this.textSendAHPID);
            this.tabPage6.Controls.Add(this.label4);
            this.tabPage6.Controls.Add(this.targetRH);
            this.tabPage6.Controls.Add(this.label5);
            this.tabPage6.Controls.Add(this.label7);
            this.tabPage6.Controls.Add(this.textCorrFactor);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Controls.Add(this.textNeededAH);
            this.tabPage6.Controls.Add(this.AFMT);
            this.tabPage6.Controls.Add(this.sendToPIDButton);
            this.tabPage6.Controls.Add(this.button1);
            this.tabPage6.Controls.Add(this.panel6);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(352, 311);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Advanced";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(119, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 20);
            this.label13.TabIndex = 31;
            this.label13.Text = "PID Calculator";
            // 
            // textSendAHPID
            // 
            this.textSendAHPID.Location = new System.Drawing.Point(123, 215);
            this.textSendAHPID.Name = "textSendAHPID";
            this.textSendAHPID.Size = new System.Drawing.Size(101, 26);
            this.textSendAHPID.TabIndex = 42;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 20);
            this.label4.TabIndex = 41;
            this.label4.Text = "PID AH INJ:";
            // 
            // targetRH
            // 
            this.targetRH.Location = new System.Drawing.Point(123, 53);
            this.targetRH.Name = "targetRH";
            this.targetRH.Size = new System.Drawing.Size(101, 26);
            this.targetRH.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 20);
            this.label5.TabIndex = 33;
            this.label5.Text = "TargetRH:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(57, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 20);
            this.label7.TabIndex = 35;
            this.label7.Text = "AFM T:";
            // 
            // textCorrFactor
            // 
            this.textCorrFactor.Location = new System.Drawing.Point(123, 125);
            this.textCorrFactor.Name = "textCorrFactor";
            this.textCorrFactor.Size = new System.Drawing.Size(101, 26);
            this.textCorrFactor.TabIndex = 40;
            this.textCorrFactor.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 20);
            this.label8.TabIndex = 36;
            this.label8.Text = "corr. factor:";
            // 
            // textNeededAH
            // 
            this.textNeededAH.Location = new System.Drawing.Point(123, 178);
            this.textNeededAH.Name = "textNeededAH";
            this.textNeededAH.Size = new System.Drawing.Size(101, 26);
            this.textNeededAH.TabIndex = 39;
            // 
            // AFMT
            // 
            this.AFMT.Location = new System.Drawing.Point(123, 89);
            this.AFMT.Name = "AFMT";
            this.AFMT.Size = new System.Drawing.Size(101, 26);
            this.AFMT.TabIndex = 38;
            // 
            // sendToPIDButton
            // 
            this.sendToPIDButton.Location = new System.Drawing.Point(143, 261);
            this.sendToPIDButton.Name = "sendToPIDButton";
            this.sendToPIDButton.Size = new System.Drawing.Size(129, 33);
            this.sendToPIDButton.TabIndex = 31;
            this.sendToPIDButton.Text = "SEND TO PID";
            this.sendToPIDButton.UseVisualStyleBackColor = true;
            this.sendToPIDButton.Click += new System.EventHandler(this.sendToPIDButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 261);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 33);
            this.button1.TabIndex = 30;
            this.button1.Text = "CALCULATE";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label9);
            this.panel6.Location = new System.Drawing.Point(6, 169);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(266, 86);
            this.panel6.TabIndex = 31;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(34, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 20);
            this.label9.TabIndex = 37;
            this.label9.Text = "need AH:";
            // 
            // tabPage9
            // 
            this.tabPage9.BackColor = System.Drawing.Color.Transparent;
            this.tabPage9.Controls.Add(this.label1);
            this.tabPage9.Controls.Add(this.textBox1);
            this.tabPage9.Controls.Add(this.atomizerStatus);
            this.tabPage9.Controls.Add(this.fanButton);
            this.tabPage9.Controls.Add(this.atomizerButton);
            this.tabPage9.Controls.Add(this.humidorCELLToggle);
            this.tabPage9.Controls.Add(this.FanStatus);
            this.tabPage9.Controls.Add(this.humidorCellActivated);
            this.tabPage9.Location = new System.Drawing.Point(4, 29);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(352, 311);
            this.tabPage9.TabIndex = 2;
            this.tabPage9.Text = "System";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 39;
            this.label1.Text = "Potentiometer:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(152, 128);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(101, 26);
            this.textBox1.TabIndex = 40;
            // 
            // atomizerStatus
            // 
            this.atomizerStatus.AutoSize = true;
            this.atomizerStatus.Location = new System.Drawing.Point(152, 99);
            this.atomizerStatus.Name = "atomizerStatus";
            this.atomizerStatus.Size = new System.Drawing.Size(133, 20);
            this.atomizerStatus.TabIndex = 36;
            this.atomizerStatus.Text = "Atomizer status: -";
            // 
            // fanButton
            // 
            this.fanButton.Location = new System.Drawing.Point(17, 53);
            this.fanButton.Name = "fanButton";
            this.fanButton.Size = new System.Drawing.Size(129, 33);
            this.fanButton.TabIndex = 33;
            this.fanButton.Text = "FAN ON/OFF";
            this.fanButton.UseVisualStyleBackColor = true;
            this.fanButton.Click += new System.EventHandler(this.fanButton_Click);
            // 
            // atomizerButton
            // 
            this.atomizerButton.Location = new System.Drawing.Point(17, 92);
            this.atomizerButton.Name = "atomizerButton";
            this.atomizerButton.Size = new System.Drawing.Size(129, 33);
            this.atomizerButton.TabIndex = 35;
            this.atomizerButton.Text = "ATO. ON/OFF";
            this.atomizerButton.UseVisualStyleBackColor = true;
            this.atomizerButton.Click += new System.EventHandler(this.atomizerButton_Click);
            // 
            // FanStatus
            // 
            this.FanStatus.AutoSize = true;
            this.FanStatus.Location = new System.Drawing.Point(152, 59);
            this.FanStatus.Name = "FanStatus";
            this.FanStatus.Size = new System.Drawing.Size(98, 20);
            this.FanStatus.TabIndex = 34;
            this.FanStatus.Text = "Fan status: -";
            // 
            // humidorCellActivated
            // 
            this.humidorCellActivated.AutoSize = true;
            this.humidorCellActivated.Location = new System.Drawing.Point(152, 20);
            this.humidorCellActivated.Name = "humidorCellActivated";
            this.humidorCellActivated.Size = new System.Drawing.Size(96, 20);
            this.humidorCellActivated.TabIndex = 31;
            this.humidorCellActivated.Text = "Cell status: -";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Location = new System.Drawing.Point(8, 78);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(429, 277);
            this.tabControl2.TabIndex = 32;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.Transparent;
            this.tabPage7.Controls.Add(this.panel1);
            this.tabPage7.Location = new System.Drawing.Point(4, 29);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(421, 244);
            this.tabPage7.TabIndex = 0;
            this.tabPage7.Text = "Sensors";
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.Color.Transparent;
            this.tabPage8.Controls.Add(this.panel7);
            this.tabPage8.Controls.Add(this.webLastUpdate);
            this.tabPage8.Location = new System.Drawing.Point(4, 29);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(421, 244);
            this.tabPage8.TabIndex = 1;
            this.tabPage8.Text = "Web";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.webPressure);
            this.panel7.Controls.Add(this.webRH);
            this.panel7.Controls.Add(this.webDP);
            this.panel7.Controls.Add(this.webT);
            this.panel7.Location = new System.Drawing.Point(7, 8);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(255, 126);
            this.panel7.TabIndex = 24;
            // 
            // webPressure
            // 
            this.webPressure.AutoSize = true;
            this.webPressure.Location = new System.Drawing.Point(8, 16);
            this.webPressure.Name = "webPressure";
            this.webPressure.Size = new System.Drawing.Size(118, 20);
            this.webPressure.TabIndex = 10;
            this.webPressure.Text = "Pression atm.: -";
            // 
            // webRH
            // 
            this.webRH.AutoSize = true;
            this.webRH.Location = new System.Drawing.Point(8, 39);
            this.webRH.Name = "webRH";
            this.webRH.Size = new System.Drawing.Size(46, 20);
            this.webRH.TabIndex = 11;
            this.webRH.Text = "RH: -";
            // 
            // webDP
            // 
            this.webDP.AutoSize = true;
            this.webDP.Location = new System.Drawing.Point(8, 85);
            this.webDP.Name = "webDP";
            this.webDP.Size = new System.Drawing.Size(44, 20);
            this.webDP.TabIndex = 13;
            this.webDP.Text = "DP: -";
            // 
            // webT
            // 
            this.webT.AutoSize = true;
            this.webT.Location = new System.Drawing.Point(8, 62);
            this.webT.Name = "webT";
            this.webT.Size = new System.Drawing.Size(31, 20);
            this.webT.TabIndex = 12;
            this.webT.Text = "T: -";
            // 
            // webLastUpdate
            // 
            this.webLastUpdate.AutoSize = true;
            this.webLastUpdate.Location = new System.Drawing.Point(3, 211);
            this.webLastUpdate.Name = "webLastUpdate";
            this.webLastUpdate.Size = new System.Drawing.Size(101, 20);
            this.webLastUpdate.TabIndex = 14;
            this.webLastUpdate.Text = "last update: -";
            // 
            // scriptStopButton
            // 
            this.scriptStopButton.Location = new System.Drawing.Point(532, 354);
            this.scriptStopButton.Name = "scriptStopButton";
            this.scriptStopButton.Size = new System.Drawing.Size(139, 45);
            this.scriptStopButton.TabIndex = 39;
            this.scriptStopButton.Text = "Stop script";
            this.scriptStopButton.UseVisualStyleBackColor = true;
            this.scriptStopButton.Click += new System.EventHandler(this.scriptStopButton_Click);
            // 
            // scriptOutputClear
            // 
            this.scriptOutputClear.Location = new System.Drawing.Point(693, 354);
            this.scriptOutputClear.Name = "scriptOutputClear";
            this.scriptOutputClear.Size = new System.Drawing.Size(139, 45);
            this.scriptOutputClear.TabIndex = 40;
            this.scriptOutputClear.Text = "Clear Output";
            this.scriptOutputClear.UseVisualStyleBackColor = true;
            this.scriptOutputClear.Click += new System.EventHandler(this.scriptOutputClear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1477, 1248);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tabcontrol);
            this.Controls.Add(this.humidorServerToUser);
            this.Controls.Add(this.humidorUserToServer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.humidorLastUpdate);
            this.Controls.Add(this.debugTextBox);
            this.Controls.Add(this.humidorDisconnect);
            this.Controls.Add(this.humidorConnect);
            this.Controls.Add(this.humidorPort);
            this.Controls.Add(this.humidorPortLabel);
            this.Controls.Add(this.humidorIPLabel);
            this.Controls.Add(this.humidorIP);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "HumidorControlPro";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidorRHChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.tabcontrol.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidorTChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidorAHChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidorPPMChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox humidorIP;
        private System.Windows.Forms.Label humidorIPLabel;
        private System.Windows.Forms.Label humidorPortLabel;
        private System.Windows.Forms.TextBox humidorPort;
        private System.Windows.Forms.Button humidorConnect;
        private System.Windows.Forms.Button humidorDisconnect;
        private System.Windows.Forms.TextBox debugTextBox;
        private System.Windows.Forms.Label humidorLastUpdate;
        private System.Windows.Forms.Label humidorSensorWETFlow;
        private System.Windows.Forms.Label humidorSensorDRYFlow;
        private System.Windows.Forms.Label humidorSensorINJRH;
        private System.Windows.Forms.Label humidorSensorINJT;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label humidorPIDActivated;
        private System.Windows.Forms.Label humidorPIDSetpoint;
        private System.Windows.Forms.Label humidorFlowsWET;
        private System.Windows.Forms.Label humidorFlowsDRY;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button humidorPIDToggle;
        private System.Windows.Forms.TextBox humidorPIDNewSetpoint;
        private System.Windows.Forms.Button humidorUserToServer;
        private System.Windows.Forms.Button humidorServerToUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataVisualization.Charting.Chart humidorRHChart;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TabControl tabcontrol;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataVisualization.Charting.Chart humidorTChart;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.DataVisualization.Charting.Chart humidorAHChart;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.Label humidorSensorEXHT;
        private System.Windows.Forms.Label humidorSensorEXHRH;
        private System.Windows.Forms.Label humidorSensorCELLT;
        private System.Windows.Forms.Label humidorSensorCELLRH;
        private System.Windows.Forms.Label humidorSensorCELLAH;
        private System.Windows.Forms.Label humidorSensorCELLDP;
        private System.Windows.Forms.Label humidorSensorEXHAH;
        private System.Windows.Forms.Label humidorSensorEXHDP;
        private System.Windows.Forms.Label humidorSensorINJAH;
        private System.Windows.Forms.Label humidorSensorINJDP;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label proportional_label;
        private System.Windows.Forms.Label integral_label;
        private System.Windows.Forms.Label derivative_label;
        private System.Windows.Forms.TextBox humidorPNew;
        private System.Windows.Forms.TextBox humidorINew;
        private System.Windows.Forms.TextBox humidorDNew;
        private System.Windows.Forms.Label humidorD;
        private System.Windows.Forms.Label humidorI;
        private System.Windows.Forms.Label humidorP;
        private System.Windows.Forms.Label humidorSensorCELLPPM;
        private System.Windows.Forms.Label humidorSensorEXHPPM;
        private System.Windows.Forms.Label humidorSensorINJPPM;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataVisualization.Charting.Chart humidorPPMChart;
        private System.Windows.Forms.TrackBar trackBar4;
        private System.Windows.Forms.Button humidorCELLToggle;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox humidorCTRLNew;
        private System.Windows.Forms.TextBox textSendAHPID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox targetRH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textCorrFactor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textNeededAH;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox AFMT;
        private System.Windows.Forms.Button sendToPIDButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label humidorCellActivated;
        private System.Windows.Forms.TextBox humidorWetNew;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox humidorDryNew;
        private System.Windows.Forms.Label humidorCTRL;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label webPressure;
        private System.Windows.Forms.Label webRH;
        private System.Windows.Forms.Label webDP;
        private System.Windows.Forms.Label webT;
        private System.Windows.Forms.Label webLastUpdate;
        private System.Windows.Forms.Label FanStatus;
        private System.Windows.Forms.Button fanButton;
        private System.Windows.Forms.Label atomizerStatus;
        private System.Windows.Forms.Button atomizerButton;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox scriptInput;
        private System.Windows.Forms.TextBox scriptOutput;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button scriptRunButton;
        private System.Windows.Forms.Button scriptStopButton;
        private System.Windows.Forms.Button scriptOutputClear;
    }
}

