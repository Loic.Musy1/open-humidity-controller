import curses
from sht_sensor import Sht
from ADCDACPi import ADCDACPi
import RPi.GPIO as GPIO
import time
import datetime
from jataruku import PID

class Humidor(object):

    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(22, GPIO.OUT)
        GPIO.output(22, False)

        self.adcdac = ADCDACPi(2)
        self.sht75inj = Sht(26, 5)
        self.sht75exh = Sht(13, 6)    

        self.uniq_filename = str(datetime.datetime.now().date()) + '_' + str(datetime.datetime.now().time()).replace(':', '.')
        self.meas_fd = open(self.uniq_filename + ".csv", "w")
        self.meas_fd.write("Time\tWET flow\tDRY flow\tINJ T\tINJ RH\tINJ DP\tEXH T\tEXH RH\tEXH DP\tPID\tPID SP\tPID OUT\n")

        self.pid_control = 0
        self.pid_setpoint = 0
        self.pid_output = 0

        self.wet = 0
        self.dry = 0

        self.injt = 0
        self.injrh = 0
        self.injdp = 0

        self.exht = 0
        self.exhrh = 0
        self.exhdp = 0

class curses_screen:
    def __enter__(self):
        self.stdscr = curses.initscr()
        curses.cbreak()
        curses.noecho()
        self.stdscr.keypad(1)
        SCREEN_HEIGHT, SCREEN_WIDTH = self.stdscr.getmaxyx()
        return self.stdscr
    def __exit__(self,a,b,c):
        curses.nocbreak()
        self.stdscr.keypad(0)
        curses.echo()
        curses.endwin()


humidor = Humidor()

humidor_injrh = lambda: humidor.injrh

def humidor_output(x):
    humidor.adcdac.set_dac_voltage(1, float(x)/100.0)
    humidor.adcdac.set_dac_voltage(2, float(329-x)/100.0)
    humidor.pid_output = x

pid = PID(humidor_injrh, humidor_output, humidor.pid_setpoint, 8, 1, 0, True)
pid.set_output_limits(0, 329)
pid.auto = True 

start_time = (datetime.datetime.now()-datetime.datetime(1970,1,1)).total_seconds()
test_ramp = 0

with curses_screen() as stdscr:


    scr = curses.initscr()
    curses.halfdelay(1)
    curses.noecho()
    

    while True:
        curses.halfdelay(1)
        char = scr.getch()
                                  # or there is input to be handled
        scr.clear()               # Clears the screen
        if char != curses.ERR:    # This is true if the user pressed something
            try:
                if chr(char) == 'q':
                    humidor.meas_fd.close()
                    curses.endwin()
                    quit()
                if chr(char) == 's':
                    scr.addstr(0,0,"RH setpoint - enter value (0 to 100):")
                    curses.echo()
                    s = scr.getstr(1,0,3)
                    curses.noecho()
                    if len(s) > 0:
                        if float(s) >= 0:
                            if float(s) <= 100:
                                humidor.pid_setpoint = float(s)
                if chr(char) == 'p':
                    scr.addstr(0,0,"PID control - 1 to enable, 0 to disable:")
                    curses.echo()
                    s = scr.getstr(1,0,1)
                    curses.noecho()
                    humidor.pid_control = int(s)
                if chr(char) == 't':
                    scr.addstr(0,0,"Test PID ramp - 1 to enable, 0 to disable:")
                    curses.echo()
                    s = scr.getstr(1,0,1)
                    curses.noecho()
                    test_ramp = int(s)
                    start_time = (datetime.datetime.now()-datetime.datetime(1970,1,1)).total_seconds()
                if chr(char) == 'd':
                    scr.addstr(0,0,"DRY flow setting - enter value in sccm (max 329):")
                    curses.echo()
                    s = scr.getstr(1,0,3)
                    curses.noecho()
                    if len(s) > 0:
                        if float(s) >= 0:
                            if float(s) < 330:
                                humidor.adcdac.set_dac_voltage(2, float(s)/100.0)
    
                if chr(char) == 'w':
                    scr.addstr(0,0,"WET flow setting - enter value in sccm (max 329):")
                    curses.echo()
                    s = scr.getstr(1,0,3)
                    curses.noecho()
                    if len(s) > 0:
                        if float(s) >= 0:
                            if float(s) < 330:
                                humidor.adcdac.set_dac_voltage(1, float(s)/100.0)
            except:
                pass
        else:
            scr.addstr(0, 0, "Humidor V3 - Control")
            scr.addstr(1, 0, "====================")
            scr.addstr(3, 0, "piMFC status:")
            humidor.wet = humidor.adcdac.read_adc_voltage(1,0)*100
            humidor.dry = humidor.adcdac.read_adc_voltage(2,0)*100
            mfc_wet_flow =   " -> WET flow: " + str(humidor.wet) + " sccm"
            mfc_dry_flow =   " -> DRY flow: " + str(humidor.dry) + " sccm"
            scr.addstr(4, 0, mfc_wet_flow)
            scr.addstr(5, 0, mfc_dry_flow)

            scr.addstr(7, 0, "SHT75 status:")
            humidor.injt = humidor.sht75inj.read_t()
            humidor.injrh = humidor.sht75inj.read_rh()
            humidor.injdp = humidor.sht75inj.read_dew_point(humidor.injt, humidor.injrh)
            sht75_inj_t =   " -> INJ T: " + str(humidor.injt) + " degC"
            sht75_inj_rh =   " -> INJ RH: " + str(humidor.injrh) + " %"
            sht75_inj_dp =   " -> INJ DP: " + str(humidor.injdp) + " degC"
            scr.addstr(8, 0, sht75_inj_t)
            scr.addstr(9, 0, sht75_inj_rh)
            scr.addstr(10, 0, sht75_inj_dp)
            humidor.exht = humidor.sht75exh.read_t()
            humidor.exhrh = humidor.sht75exh.read_rh()
            humidor.exhdp = humidor.sht75exh.read_dew_point(humidor.exht, humidor.exhrh)
            sht75_exh_t =   " -> EXH T: " + str(humidor.exht) + " degC"
            sht75_exh_rh =   " -> EXH RH: " + str(humidor.exhrh) + " %"
            sht75_exh_dp =   " -> EXH DP: " + str(humidor.exhdp) + " degC"
            scr.addstr(11, 0, sht75_exh_t)
            scr.addstr(12, 0, sht75_exh_rh)
            scr.addstr(13, 0, sht75_exh_dp)
            
            scr.addstr(16, 0, "Press Q to quit, W/D to change WET/DRY flow, P to toggle PID, S for PID setpoint")
            scr.addstr(17, 0, "      T for test waveform")
            scr.addstr(18, 0, " -> Recording data to " + humidor.uniq_filename + ".csv")
            humidor.meas_fd.write(str(datetime.datetime.now()) + "\t" + str(humidor.wet) + "\t" + str(humidor.dry) + "\t" + str(humidor.injt) + "\t" + str(humidor.injrh) + "\t" + str(humidor.injdp) + "\t" + str(humidor.exht) + "\t" + str(humidor.exhrh) + "\t" + str(humidor.exhdp) + str(humidor.pid_control) + "\t" + str(humidor.pid_setpoint) + "\t" + str(humidor.pid_output) + "\n")
        if humidor.pid_control == 1:
            pid.compute()
            curr_time = (datetime.datetime.now()-datetime.datetime(1970,1,1)).total_seconds()
            if test_ramp == 1:
                delta_t = curr_time-start_time
                setpoint = 5*int(delta_t/7200)
                pid._setpoint = setpoint
                humidor.pid_setpoint = setpoint
                scr.addstr(21, 0, "/!\\ TEST RAMP ENABLED - Next increment in " + str(7200-delta_t%7200) + " seconds. /!\\")
            else:
                pid._setpoint = humidor.pid_setpoint
            scr.addstr(20, 0, "/!\\ PID CONTROL ENABLED - SETPOINT: " + str(humidor.pid_setpoint) + "% OUTPUT: " + str(humidor.pid_output) + "sccm. /!\\")
