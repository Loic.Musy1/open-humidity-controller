from sht_sensor import Sht
from ADCDACPi import ADCDACPi
import RPi.GPIO as GPIO

class Humidor(object):
    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(25, GPIO.OUT)
        GPIO.output(25, False)

        self.adcdac = ADCDACPi(2)
        self.adcdac_busy = 0
        self.sht75inj = Sht(26, 5)
        self.sht75exh = Sht(13, 6)
        self.sht75cell = Sht(3, 2)

        self.pid_control = 0
        self.pid_setpoint = 0
        self.pid_output = 0

        self.wet = 0
        self.dry = 0

        self.injt = 0
        self.injrh = 0
        self.injdp = 0

        self.exht = 0
        self.exhrh = 0
        self.exhdp = 0

        self.cellt = 0
        self.cellrh = 0
        self.celldp = 0

def humidor_output(x):
    humidor = Humidor()
    humidor.adcdac.set_dac_voltage(1, float(x) / 100.0)
    humidor.adcdac.set_dac_voltage(2, float(329 - x) / 100.0)
    humidor.pid_output = x