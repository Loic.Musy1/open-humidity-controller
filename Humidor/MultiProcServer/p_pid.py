import time
from f_pid_config import PID

# Used to send the setpoint and P,I,D values.
def PIDControl(data):
    max_voltage = 329.0

    # parse the value of the control_signal, to know the feedback variable of the PID
    def pid_controlsignal():
        cs = data["system"]["PID"]["control_signal"].value.split(".")
        return data[cs[0]][cs[1]][cs[2]].value

    # Set the ouput of the flows
    def pid_output(x):
        data["flows"]["wet"].value = float(x)
        data["flows"]["dry"].value = max_voltage-float(x)

    # Initialize the PID with the initials values
    pid = PID(pid_controlsignal, pid_output,
              data["system"]["PID"]["setpoint"].value,
              data["system"]["PID"]["proportional"].value, data["system"]["PID"]["integral"].value,
              data["system"]["PID"]["derivative"].value, True)

    pid.set_output_limits(0, max_voltage)

    while True:
        time.sleep(0.5)
        if data["system"]["PID"]["activated"].value == 1:
            # print("PID starting...")
            try:
                # Change the values of the PID and compute the output
                pid._setpoint = data["system"]["PID"]["setpoint"].value
                pid.set_tunings(data["system"]["PID"]["proportional"].value, data["system"]["PID"]["integral"].value,data["system"]["PID"]["derivative"].value)
                pid.auto = True
                pid.compute()
            except:
                print("Error in the PID computation, PID shutdown")
                data["system"]["PID"]["activated"].value = 0
        else:
            pass
            # print("PID OFF")