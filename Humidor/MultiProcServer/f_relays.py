import spidev
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)

def switchPin(pin):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(0.3)
    GPIO.output(pin, GPIO.LOW)


def setRelays(data):
    initValue_fans = data["system"]["relays"]["fans"].value
    initValue_atomizer = data["system"]["relays"]["atomizer"].value

    while True:
        if data["system"]["relays"]["fans"].value != initValue_fans:
            if data["system"]["relays"]["fans"].value == 1:
                switchPin(23)
            else:
                switchPin(24)
            initValue_fans = data["system"]["relays"]["fans"].value

        if data["system"]["relays"]["atomizer"].value != initValue_atomizer:
            if data["system"]["relays"]["atomizer"].value == 1:
                switchPin(27)
            else:
                switchPin(22)
            initValue_atomizer = data["system"]["relays"]["atomizer"].value

def setResistance(data):
    spi = spidev.SpiDev()
    spi.open(1, 0)
    spi.max_speed_hz = 1000000
    initRes = data["system"]["digipot"]["resistance"].value

    while True:
        if data["system"]["digipot"]["resistance"].value != initRes:
            initRes = data["system"]["digipot"]["resistance"].value
            resistanceIn8Bit = int(min(255,data["system"]["digipot"]["resistance"].value * (255.0 / 10000.0)))
            spi.xfer([0, resistanceIn8Bit])
