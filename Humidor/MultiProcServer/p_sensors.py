import f_humidor
import time
import numpy as np

# Variables for the formula of the AH and PPMv
Tc = 647.096
Pc = 220640
C = 216.679
C1 = -7.85951783
C2 = 1.84408259
C3 = -11.7866497
C4 = 22.6807411
C5 = -15.9618719
C6 = 1.80122502

# The formulas for AH and PPMv comes from :
# http://www.vaisala.com/Vaisala%20Documents/Application%20notes/Humidity_Conversion_Formulas_B210973EN-F.pdf
def SensorAcquisition(data):
    humidor = f_humidor.Humidor()
    while True:
        time.sleep(2)
        print("Acquisition of the sensors...")
        try:
            # Get the values of the exh, inj and cell (if activated) using the sht75 libraries
            if data["system"]["activation"]["exh"].value == 1:
                data["sensors"]["exh"]["T"].value = float(humidor.sht75exh.read_t())
                data["sensors"]["exh"]["RH"].value = float(humidor.sht75exh.read_rh())
                data["sensors"]["exh"]["DP"].value = float(humidor.sht75exh.read_dew_point(float(humidor.sht75exh.read_t()), float(humidor.sht75exh.read_rh())))

                T = data["sensors"]["exh"]["T"].value + 273.15
                theta = 1 - T/Tc
                Pws = Pc * np.exp(
                    Tc / T * (C1 * theta + C2 * pow(theta, 1.5) + C3 * pow(theta, 3) + C4 * pow(theta, 3.5)
                              + C5 * pow(theta, 4) + C6 * pow(theta, 7.5)))
                Pw = Pws * data["sensors"]["exh"]["RH"].value / 100

                data["sensors"]["exh"]["AH"].value = C * Pw / T
                data["sensors"]["exh"]["ppmv"].value = Pw / (data["sensors"]["web"]["pressure"].value - Pw) * pow(10, 6)
            else:
                data["sensors"]["exh"]["T"].value = 0
                data["sensors"]["exh"]["RH"].value = 0
                data["sensors"]["exh"]["DP"].value = 0
                data["sensors"]["exh"]["AH"].value = 0
                data["sensors"]["exh"]["ppmv"].value = 0


            if data["system"]["activation"]["inj"].value == 1:
                data["sensors"]["inj"]["T"].value = float(humidor.sht75inj.read_t())
                data["sensors"]["inj"]["RH"].value = float(humidor.sht75inj.read_rh())
                data["sensors"]["inj"]["DP"].value = float(humidor.sht75inj.read_dew_point(float(humidor.sht75inj.read_t()), float(humidor.sht75inj.read_rh())))

                T = data["sensors"]["inj"]["T"].value + 273.15
                theta = 1 - T/Tc
                Pws = Pc * np.exp(
                    Tc / T * (C1 * theta + C2 * pow(theta, 1.5) + C3 * pow(theta, 3) + C4 * pow(theta, 3.5)
                              + C5 * pow(theta, 4) + C6 * pow(theta, 7.5)))
                Pw = Pws * data["sensors"]["inj"]["RH"].value / 100

                data["sensors"]["inj"]["AH"].value = C * Pw / T
                data["sensors"]["inj"]["ppmv"].value = Pw / (data["sensors"]["web"]["pressure"].value - Pw) * pow(10, 6)
            else:
                data["sensors"]["inj"]["T"].value = 0
                data["sensors"]["inj"]["RH"].value = 0
                data["sensors"]["inj"]["DP"].value = 0
                data["sensors"]["inj"]["AH"].value = 0
                data["sensors"]["inj"]["ppmv"].value = 0

            if data["system"]["activation"]["cell"].value == 1:
                data["sensors"]["cell"]["T"].value = float(humidor.sht75cell.read_t())
                data["sensors"]["cell"]["RH"].value = float(humidor.sht75cell.read_rh())
                data["sensors"]["cell"]["DP"].value = float(
                    humidor.sht75cell.read_dew_point(float(humidor.sht75cell.read_t()), float(humidor.sht75cell.read_rh())))

                T = data["sensors"]["cell"]["T"].value + 273.15
                theta = 1 - T/Tc
                Pws = Pc * np.exp(
                    Tc / T * (C1 * theta + C2 * pow(theta, 1.5) + C3 * pow(theta, 3) + C4 * pow(theta, 3.5)
                              + C5 * pow(theta, 4) + C6 * pow(theta, 7.5)))
                Pw = Pws * data["sensors"]["cell"]["RH"].value / 100

                data["sensors"]["cell"]["AH"].value = C * Pw / T
                data["sensors"]["cell"]["ppmv"].value = Pw / (data["sensors"]["web"]["pressure"].value - Pw) * pow(10, 6)
            else:
                data["sensors"]["cell"]["T"].value = 0
                data["sensors"]["cell"]["RH"].value = 0
                data["sensors"]["cell"]["DP"].value = 0
                data["sensors"]["cell"]["AH"].value = 0
                data["sensors"]["cell"]["ppmv"].value = 0
                data["sensors"]["cell"]["ppmv"].value = 0

            # Read the values of the voltages of the two flows controlers.
            while data["system"]["adcdac_busy"].value == 1:
                time.sleep(0.05)
            data["system"]["adcdac_busy"].value = 1
            if data["system"]["activation"]["wet"].value == 1:
                data["sensors"]["flows"]["wet"].value = float(humidor.adcdac.read_adc_voltage(1, 0)*100)
            else:
                data["sensors"]["flows"]["wet"].value = 0

            if data["system"]["activation"]["dry"].value == 1:
                data["sensors"]["flows"]["dry"].value = float(humidor.adcdac.read_adc_voltage(2, 0)*100)
            else:
                data["sensors"]["flows"]["dry"].value = 0

            data["system"]["adcdac_busy"].value = 0
            print("Success")
        except:
            print("Failure")