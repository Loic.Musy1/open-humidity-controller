from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import os
import datetime
import time
from multiprocessing import Process, Manager, Value

import f_parsing

# Get the IP of the pi used inside the Humidor.
f = os.popen('ifconfig eth0 | grep "inet\ addr" | cut -d: -f2 | cut -d" " -f1')
HOST_NAME=f.read().strip()
f.close()
PORT_NUMBER = 9000 


# Web handles used to changes the variables inside the parameters dictionnary
# An url is send to the server and then parsed by the program.

def GenerateMyHandler(parameters):
    class MyHandler(BaseHTTPRequestHandler, object):
        def __init__(self, *args, **kwargs):
            self.data = parameters
            super(MyHandler, self).__init__(*args, **kwargs)
            self.data = parameters

        def do_HEAD(s):
            s.send_response(200)
            s.send_header("Content-type", "text/html")
            s.end_headers()

        def do_GET(s):
            """Respond to a GET request."""
            s.send_response(200)
            s.send_header("Content-type", "text/html")
            s.end_headers()
            s.wfile.write("<html><head><title>Title goes here.</title></head>")
            s.wfile.write("<body>")

            command = s.path
            command = command.split('/')

            # parsing of the path send to the web server.
            try:
                if command[1] == "flows":
                    if command[-1] == "get":
                        s.wfile.write("<p>GET VALUE : </p>")
                        s.wfile.write("<p>Value of " + command[2] + " " + command[1] + " is : " + str(
                            s.data[command[1]][command[2]].value) + "</p>")

                    elif command[-2] == "set":  # and command[-1].isdigit():
                        s.wfile.write("<p>SET VALUE : </p>")
                        s.wfile.write(
                            "<p>Value of " + command[2] + " " + command[1] + " set to : " + str(command[-1]) + "</p>")

                        s.data[command[1]][command[2]].value = float(command[-1])

                elif command[1] == "sensors":
                    if command[-1] == "get":
                        s.wfile.write("<p>GET VALUE : </p>")
                        s.wfile.write("<p>Current " + str(command[3]) + " value from " + str(command[2])
                                      + " " + str(command[1]) + " is: " +
                                      str(s.data[command[1]][command[2]][command[3]].value) + "</p>")
                elif command[1] == "system":
                    if command[-2] == "set":
                        try:
                            tmp = float(command[-1])
                        except:
                            tmp = str(command[-1])
                        s.data[command[1]][command[2]][command[3]].value = tmp

                elif command[1] == "parameters":
                    p = {}
                    p = f_parsing.dict_prepare(parameters, p)
                    p["time"] = str(datetime.datetime.now().time())
                    s.wfile.write(p)

            except:
                s.wfile.write("<p>Error in the parsing of the command</p>")

            s.wfile.write("</body></html>")

    return MyHandler


def info(title):
    print(title)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())



def WebServer(data):
    # info('function WebServer')
    HOST_NAME = data['connection']['ipadress'].value
    PORT_NUMBER = int(data['connection']['port'].value)
    server_class = HTTPServer
    MyHandler = GenerateMyHandler(data)
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print(time.asctime() + "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime() + "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))

