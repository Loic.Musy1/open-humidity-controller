import urllib2
import pandas as pd
import time

# Get the value of the pressure/temperature from the web. Used to calculate PPMv and AH.
def WebPressureAcquisition(data):
    while True:
        print("Acquisition of the atmospheric pressure from the Web...")
        try:
            # Get the webpage
            page = urllib2.urlopen("http://grosminet.rosset.org/temperature/temp.shtml")
            html = page.read()
            page.close()

            # Parse the html file to get the pressure
            html = html.decode("utf-8", errors='ignore')
            pressure = html.split("Pression: ")[1].split(" hPa")[0]
            DP = html.split("rose:")[1].split("Celsius")[0]
            RH = html.split("relative:")[1].split("%")[0]
            T = html.split("Temprature:")[1].split("Celsius")[0]
            last_update = html.split('<b>Dernire mise  jour:</b>')[1].split('</font>')[0]
            last_update = html.split('<font class="text-gree2">')[1].split('</font>')[0]
            last_update = pd.to_datetime(last_update)

            data["sensors"]["web"]["pressure"].value = float(pressure)
            data["sensors"]["web"]["DP"].value = float(DP)
            data["sensors"]["web"]["RH"].value = float(RH)
            data["sensors"]["web"]["T"].value = float(T)
            data["sensors"]["web"]["last_update"].value = str(last_update)

            print("Success")
        except:
            print("Failure")
        time.sleep(60)